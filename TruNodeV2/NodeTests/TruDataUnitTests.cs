﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace NodeTests
{
 
    [TestClass]
    public class TruDataUnitTests
    {
        [TestMethod]
        public void NewTruWallet()
        {
            var newTruWallet = new TruWallet();

            newTruWallet.PushNotifToken = ":alskdjflaskdjfladflaj2o3iej203";
            newTruWallet.ProfilePicBase64 = "lakoiw3g0923i0oaoisdjas";
            newTruWallet.Handle = "lkasdjf";
            newTruWallet.FullName = "Test1";

            var serializedTruWallet = JsonConvert.SerializeObject(newTruWallet);
            Debug.WriteLine(serializedTruWallet);
            var deserializedWallet = JsonConvert.DeserializeObject<TruWallet>(serializedTruWallet);
            Assert.AreEqual(newTruWallet.Keys.PubKey, deserializedWallet.Keys.PubKey);
          //  Assert.AreEqual(newTruWallet.Keys.PrivKey, deserializedWallet.Keys.PrivKey);  // We dont ever serialize the private key!!!
           
            Assert.AreEqual(newTruWallet.PushNotifToken, deserializedWallet.PushNotifToken);
            Assert.AreEqual(newTruWallet.ProfilePicBase64, deserializedWallet.ProfilePicBase64);
            Assert.AreEqual(newTruWallet.Handle, deserializedWallet.Handle);
            Assert.AreEqual(newTruWallet.FullName, deserializedWallet.FullName);

        }


        [TestMethod]
        public void SaveTruWallet()
        {
            var newTruWallet = new TruWallet();
            newTruWallet.PushNotifToken = ":alskdjflaskdjfladflaj2o3iej203";
            newTruWallet.ProfilePicBase64 = "lakoiw3g0923i0oaoisdjas";
            newTruWallet.Handle = "lkasdjf";
            newTruWallet.FullName = "Test1";
            // newTruWallet.SendOut(); // Call send out to strip Sensitive data.. i.e. private key;
            var serializedTruWallet = JsonConvert.SerializeObject(newTruWallet);
            FileOperations.WriteStringToFile(serializedTruWallet, "walletInfo.json");

            var stringData = FileOperations.ReadToString("walletInfo.json");

            var loadWallet = JsonConvert.DeserializeObject<TruWallet>(stringData);
            Assert.AreEqual(newTruWallet.Keys.PubKey, loadWallet.Keys.PubKey);
            // Assert.AreEqual(newTruWallet.Keys.PrivKey, loadWallet.Keys.PrivKey);   // We dont ever serialize the private key!!!

            Assert.AreEqual(newTruWallet.PushNotifToken, loadWallet.PushNotifToken);
            Assert.AreEqual(newTruWallet.ProfilePicBase64, loadWallet.ProfilePicBase64);
            Assert.AreEqual(newTruWallet.Handle, loadWallet.Handle);
            Assert.AreEqual(newTruWallet.FullName, loadWallet.FullName);
        }


    }
}
