﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruNodeV2.TruConsensus;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace NodeTests
{
    [TestClass]
    public class TruConsensusTests
    {

        private ConsensusManager ConsensusManager;

        private TruWallet MainWallet = null;

        private void Initialize()
        {
            MainWallet = new TruWallet();
            MainWallet.FullName = "Truasis Network";
            MainWallet.Handle = "truasis";

            var NetworkWallets = new TruWalletManager();
            NetworkWallets.Add(MainWallet.ID, MainWallet);

            ConsensusManager = new ConsensusManager(MainWallet, NetworkWallets);

        }

        [TestMethod]
        public void CreateVote()
        {
            Initialize();
            var anotherWallet = new TruWallet();
            anotherWallet.FullName = "Billy";
            anotherWallet.Handle = "bobber";

             
            var vote = ConsensusManager.LeaderElectionPool.Vote(0, "");


            //AnoterWallet 
            var anotherSetNetworkWallets = new TruWalletManager();
            anotherSetNetworkWallets.Add(MainWallet.ID, MainWallet);

           var anotherConsensusManager = new ConsensusManager(anotherWallet, anotherSetNetworkWallets);

            anotherConsensusManager.LeaderElectionPool.AddVote(vote);

            Assert.AreEqual(anotherConsensusManager.LeaderElectionPool.Pool.Count, 1);
            Assert.AreEqual(ConsensusManager.LeaderElectionPool.Pool.Count, 1);
            Assert.IsTrue(vote.isValidSig(anotherSetNetworkWallets[MainWallet.ID].Keys.PubKey));

        }


       



        [TestMethod]
        public void PrePrepare()
        {
            Initialize();

            var block = new TruBlock(0, new byte[0]);

            var prepare = ConsensusManager.PreparePool.Prepare(block);

            var anotherWallet = new TruWallet();
            anotherWallet.FullName = "Billy";
            anotherWallet.Handle = "bobber";

 
            //AnoterWallet 
            var anotherSetNetworkWallets = new TruWalletManager();
            anotherSetNetworkWallets.Add(MainWallet.ID, MainWallet);

            Assert.AreEqual(ConsensusManager.PreparePool.Pool.Count, 1);
            Assert.IsTrue(prepare.isValidSig(anotherSetNetworkWallets[MainWallet.ID].Keys.PubKey));

        }

        [TestMethod]
        public void Commit()
        {
            Initialize();

            var block = new TruBlock(0, new byte[0]);

            var commit = ConsensusManager.CommitPool.Commit(block);

            var anotherWallet = new TruWallet();
            anotherWallet.FullName = "Billy";
            anotherWallet.Handle = "bobber";
             
            //AnoterWallet 
            var anotherSetNetworkWallets = new TruWalletManager();
            anotherSetNetworkWallets.Add(MainWallet.ID, MainWallet);

            Assert.AreEqual(ConsensusManager.CommitPool.Pool.Count, 1);
            Assert.IsTrue(commit.isValidSig(anotherSetNetworkWallets[MainWallet.ID].Keys.PubKey));
        }

    }
}
