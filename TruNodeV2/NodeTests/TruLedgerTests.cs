using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace NodeTests
{
    [TestClass]
    public class TruLedgerTests
    {
        [TestMethod]
        public void TestMethod1()
        {

            var truasisWallet = new TruWallet();
            string truasisWalletId = truasisWallet.ID;
            var truLedger = new TruLedger(truasisWallet, new TruV2Shared.TruDataUnits.TruWalletManager());

            var expectedTruWalletAmount = 100000;
            var aNewGuy1 = new TruWallet();

            var firstTx = new TruTx()
            {
                ReceiverId = aNewGuy1.ID,
                SignerId = truasisWalletId,
                TruAmount = 1000
            };

            firstTx = TruRSACrypto.SignMsg(firstTx, truasisWallet.Keys.PrivKey) as TruTx;

            firstTx.ExpectedValidationResponses = 0;

            Assert.IsTrue(truLedger.AddNewTx(firstTx));

            expectedTruWalletAmount = expectedTruWalletAmount - 1000;
            var aNewGuy2 = new TruWallet();

            var secondTx = new TruTx()
            {
                ReceiverId = aNewGuy2.ID,
                SignerId = truasisWalletId,
                TruAmount = 100
            };

            secondTx = TruRSACrypto.SignMsg(secondTx, truasisWallet.Keys.PrivKey) as TruTx;


            Assert.IsTrue(truLedger.AddNewTx(secondTx));

            expectedTruWalletAmount = expectedTruWalletAmount - 100;


            var thirdTx = new TruTx()
            {
                ReceiverId = aNewGuy2.ID,
                SignerId = aNewGuy1.ID,
                TruAmount = 300
            };

            thirdTx = TruRSACrypto.SignMsg(thirdTx, aNewGuy1.Keys.PrivKey) as TruTx;



            Assert.IsTrue(truLedger.AddNewTx(thirdTx));

            Console.WriteLine(truLedger.Chain.Count);
            Assert.AreEqual(2, truLedger.Chain.Count);
            Assert.AreEqual(1, truLedger.Chain[0].Transactions.Count);
            Assert.AreEqual(3, truLedger.Chain[1].Transactions.Count);

            var balances = truLedger.CrawlChain();
            balances = truLedger.BalancesWithLastBlock(balances);
            Assert.AreEqual(400, balances[aNewGuy2.ID]);

            Assert.AreEqual(expectedTruWalletAmount, balances[truasisWalletId]);

            Assert.AreEqual(700, balances[aNewGuy1.ID]);
        }


        [TestMethod]
        public void TestMethod2()
        {
            var truasisWallet = new TruWallet();
            string truasisWalletId = truasisWallet.ID;
            var truLedger = new TruLedger(truasisWallet, new TruV2Shared.TruDataUnits.TruWalletManager());

            string aNewGuy1 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy1,
                SignerId = truasisWalletId,
                TruAmount = 1000
            });


            string aNewGuy2 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = truasisWalletId,
                TruAmount = 100
            });

            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = aNewGuy1,
                TruAmount = 300
            });



            var balancesAtThisPoint = truLedger.CrawlChain();



            truLedger.AppendChain();

            string aNewGuy3 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy3,
                SignerId = truasisWalletId,
                TruAmount = 2000
            });


            var myLedger = new TruLedger(truLedger.Chain);


            var otherPersonsBalanceSheet = truLedger.GetLatestBalances();

            var myBalancesAtThisPoint = myLedger.GetLatestBalances();

            foreach (var k in otherPersonsBalanceSheet)
            {
                Assert.AreEqual(k.Value, myBalancesAtThisPoint[k.Key]);
            }

        }


        [TestMethod]
        public void TestMethod3()
        {
            var truasisWallet = new TruWallet();
            string truasisWalletId = truasisWallet.ID;
            var truLedger = new TruLedger(truasisWallet, new TruV2Shared.TruDataUnits.TruWalletManager());
            var expectedTruWalletAmount = 100000;

            string aNewGuy1 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy1,
                SignerId = truasisWalletId,
                TruAmount = 1000
            });

            expectedTruWalletAmount = expectedTruWalletAmount - 1000;
            string aNewGuy2 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = truasisWalletId,
                TruAmount = 100
            });
            expectedTruWalletAmount = expectedTruWalletAmount - 100;
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = aNewGuy1,
                TruAmount = 300
            });

            var balances = truLedger.CrawlChain();

            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = aNewGuy1,
                TruAmount = 100
            });

            balances = truLedger.CrawlChain();
            balances = truLedger.CrawlChain();
        }



        [TestMethod]
        public void PersistTruLedger()
        {
            var truasisWallet = new TruWallet();
            string truasisWalletId = truasisWallet.ID;
            var truLedger = new TruLedger(truasisWallet, new TruV2Shared.TruDataUnits.TruWalletManager());

            string aNewGuy1 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy1,
                SignerId = truasisWalletId,
                TruAmount = 1000
            });


            string aNewGuy2 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = truasisWalletId,
                TruAmount = 100
            });

            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy2,
                SignerId = aNewGuy1,
                TruAmount = 300
            });



            var balancesAtThisPoint = truLedger.CrawlChain();



            truLedger.AppendChain();

            string aNewGuy3 = Guid.NewGuid().ToString();
            truLedger.AddNewTx(new TruTx()
            {
                ReceiverId = aNewGuy3,
                SignerId = truasisWalletId,
                TruAmount = 2000
            });

            truLedger.PersistAll();
            truLedger.PersistBlock(2);
        }

        [TestMethod]
        public void IsNewLedger()
        {
            //Clear Persist:
            TruLedger.ClearPersistance();
            var truLedger = new TruLedger(new TruV2Shared.TruDataUnits.TruWalletManager());
            Assert.IsTrue(truLedger.isNewNode);
            var truasisWallet = new TruWallet();
            string truasisWalletId = truasisWallet.ID;
            var initMasterLedger = new TruLedger(truasisWallet, new TruV2Shared.TruDataUnits.TruWalletManager());
            Assert.IsFalse(initMasterLedger.isNewNode);
 
        }


        [TestMethod]
        public void LoadLedger()
        {
            var truLedger = new TruLedger(new TruV2Shared.TruDataUnits.TruWalletManager());
            truLedger.LoadChain();
            if (truLedger.isNewNode)
            {
                //Get Data From Master!!!!

            }
            else
            {
                //Get partial latest data..
                
                //Get ledger runout..

              

            }

        }

    }
}
