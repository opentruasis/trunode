﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.Interfaces;
using TruV2Shared.TruDataUnits;

namespace NodeTests
{
    [TestClass]
    public class CryptoTests
    {

        [TestMethod]
        public void Base64EncodeTesting()
        {
            byte[] data = new byte[256];

            for(int i=1; i<data.Length; i++)
            {
                data[i] = (byte)i;
                Debug.WriteLine(data[i]);
            }

            var encodedString = Convert.ToBase64String(data);

            Debug.WriteLine(encodedString);

        }

        [TestMethod]
        public WalletKeys EncodeWalletKeys()
        {
           var keys = TruRSACrypto.GenerateNewKeys();
 
            Debug.WriteLine(keys);

            return keys;
        }
 
 
        [TestMethod]
        public void CreateWallet()
        {
            var newWalletKeys = TruRSACrypto.GenerateNewKeys();
            FileOperations.WriteStringToFile(JsonConvert.SerializeObject(newWalletKeys), "walletInfo.json");
            var stringData = FileOperations.ReadToString("walletInfo.json");
            var loadedWalletKeys = JsonConvert.DeserializeObject<WalletKeys>(stringData);

     
            Assert.AreEqual(loadedWalletKeys.PubKey, newWalletKeys.PubKey);
            //  Assert.AreEqual(loadedWalletKeys.PrivKey, newWalletKeys.PrivKey);   // We dont ever serialize the private key!!!

            Assert.AreEqual(loadedWalletKeys.PrivKey, "");
        }


        [TestMethod]
        public void EncryptAndDecrypt()
        {
            var stringData = FileOperations.ReadToString("walletInfo.json");
            var loadedWalletKeys = JsonConvert.DeserializeObject<WalletKeys>(stringData);
 
        
            var stringToEncrypt = "asdfasdfasdfasdfasdfasdf";


            var otherWallet = TruRSACrypto.GenerateNewKeys();

            var encryptedBytes = TruRSACrypto.Encrypt(Encoding.ASCII.GetBytes(stringToEncrypt), otherWallet.PubKey);


            Debug.WriteLine(Encoding.ASCII.GetString(encryptedBytes));


            //now decrypt

            var plainStringBytes = TruRSACrypto.Decrypt(encryptedBytes, otherWallet.PrivKey);

            var plainStrting = Encoding.ASCII.GetString(plainStringBytes);
            Debug.WriteLine(plainStrting);

            Assert.AreEqual(stringToEncrypt, plainStrting);

        }


        public class TestSignedMsg : SignedMsg
        {
            public TestSignedMsg(string data)
            {
                this.msg = data;
            }
            private string msg;
     
            public override string stringifyMsg()
            {
                return this.msg;
            }
        }

        [TestMethod]
        public void SimpleSigTest()
        {
            var thisWalletKeys = new WalletKeys()
            {
                PrivKey = "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAFBNVTXi72f4FHVELc2U+vn4Ad45O1iKlJiHfm9jC2V/5amQaCBg/JNB7a6EtCcQFspazlDWiHU5Cx9cJdx7tmuONh9ocbyNnpw6efdg01MuFU04WtBwg+ZQ+Yci6pkPcUUYADyniexHOX2oZNISzXEyNG3kReR2j4Pd+03kwyeidhqc4jlhiO1lepIy7/4xc9BNTDCO2nVtr7yRADNm6QHB/KRlyV5+5+CHrs9dURZfT/HIB5H0DTH6H3b0mXkdMe5jALTvVFs6sHWFKE8AkxuABuOmhzzsXmeN/smnlNEnPrP8ryyQO2/ULy7wmAI1KuXSagBOgNL34t2oZueYFUdPEPYjISuB/+nEajX5EuYehCEiWFtPhmfXCkGutqGvvSeR/Q3DN6dLVwExstkfnYVJM+EqCoDVs9cdL/kJS+rPbNKPmUZ17d2NK1Hq/EPi4jzY61s77Lu+ETRdUcA0BdJ1PXhaUKZiOJIAscvcJq33cLHcqcUxqCnNt0AFh54mWUHM3fjKRUjChWvXci/arPLSsFWIoual4SmvxLBAynteXVKqAKFbpZ5l/Jv8dzENbpPoKB0Z1r/lRlFSWwE9t75",
                PubKey = "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAE="
            };


            var privRSAParam = TruRSACrypto.DeserializePrivKey(Convert.FromBase64String(thisWalletKeys.PrivKey));
            // Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
            // to specify the use of SHA1 for hashing.

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(privRSAParam);


            var msgString = "asdf";

            byte[] msgSerializedBytes = Encoding.ASCII.GetBytes(msgString);

            byte[] msgHash = TruRSACrypto.Sha1Hash(msgSerializedBytes);


            byte[] signedData = rsa.SignData(msgHash, CryptoConfig.MapNameToOID("SHA1"));


            string sigHex = TruRSACrypto.ByteArrayToHexString(signedData);

            Debug.WriteLine(sigHex);

        }


        [TestMethod]

        public void TestInteropSignandVerify()
        {
            byte[] testBuf = new byte[5] { 1, 2, 0, 0, 0 };

            string testBufHex = TruRSACrypto.ByteArrayToHexString(testBuf);

            

            var thisWalletKeys = new WalletKeys(){
                   PrivKey = "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAFBNVTXi72f4FHVELc2U+vn4Ad45O1iKlJiHfm9jC2V/5amQaCBg/JNB7a6EtCcQFspazlDWiHU5Cx9cJdx7tmuONh9ocbyNnpw6efdg01MuFU04WtBwg+ZQ+Yci6pkPcUUYADyniexHOX2oZNISzXEyNG3kReR2j4Pd+03kwyeidhqc4jlhiO1lepIy7/4xc9BNTDCO2nVtr7yRADNm6QHB/KRlyV5+5+CHrs9dURZfT/HIB5H0DTH6H3b0mXkdMe5jALTvVFs6sHWFKE8AkxuABuOmhzzsXmeN/smnlNEnPrP8ryyQO2/ULy7wmAI1KuXSagBOgNL34t2oZueYFUdPEPYjISuB/+nEajX5EuYehCEiWFtPhmfXCkGutqGvvSeR/Q3DN6dLVwExstkfnYVJM+EqCoDVs9cdL/kJS+rPbNKPmUZ17d2NK1Hq/EPi4jzY61s77Lu+ETRdUcA0BdJ1PXhaUKZiOJIAscvcJq33cLHcqcUxqCnNt0AFh54mWUHM3fjKRUjChWvXci/arPLSsFWIoual4SmvxLBAynteXVKqAKFbpZ5l/Jv8dzENbpPoKB0Z1r/lRlFSWwE9t75",
                   PubKey = "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAE="
            };


            var testMsg = new TestSignedMsg("{\"TruAmount\":\"222.000000\",\"SenderId\":\"4c141955-b201-41b9-bb4e-a4b7d1ace5e5\",\"ReceiverId\":\"100703b9-ef4e-43ea-80bb-0b644da8a23f\",\"ReceiverPubKey\":\"4X+Jp5tZAWnL2lC4c6z6N0oeNkU4CLol+QpFBnBArHdSkOZCOd8Bx5NkiVxUdI4Vzc0a48RbAAltzqwXQgPvB5ecvox7svvbkIvipqv4xMrrknJWU1XgaW9ue1rZkyvVLt4/RXOlUFW9lEjzwe09sxYFlRN6vMzeJCcCjLfrnasBAAE=\"}");
 
            var results =   TruRSACrypto.SignMsg(testMsg, thisWalletKeys.PrivKey);

            Debug.WriteLine(results);

            //            {
            //                "msg": "asdf",
            //    "Signature": "c2bfc2b161282a36c39ec2ba76c3830cc397c2a4c2957b1509c29bc3bcc29d56c3af4872c2b4c2a8c28d4dc3bac2b17844c2aa0c23c39212737f2d3cc2bb6b473713c2a618c2aac387c2b84ac285c3b566c2847ec294c2af14c3be73c3a0c3ab5124c380c2912a2ac28f424bc2817b6f18c2b7c38fc3ac12c38bc2865d02c3b5c38ac3bb6dc39b750d29c281c2a561556755c2a1c2b0c38625c3b5c3964f26c281c3b0c2ad535651c29a19c2b10fc29a41c3a0c3906c285ec39b017bc3b4",
            //    "Hash": "f0e4c2f76c58916ec258f246851bea091d14d4247a2fc3e18694461b1816e13b"
            //}


            //mod  "zH/tXR1BdHK/amwJYXS9o8z1vcFUHrOfkNPfSVp7ceMkKKFLHWfNcxKZRW3cpWTQNRd9FoVhHcB82t+Dl5iRFDsDsLw5hhmECnsceGdkC0Y4f2DgUrU9LI0oBHsysQGDKCcwel5EnZTPmoVMJ4i+sGZ/AGgNZIb31gE6nXfzo9M="

            //exp     "AQAB"


            //  var PrivKey = "x8Rb9aIR79hO1w31bFKfetxeT48J4ymBZeHMqKO/OfmOvnRChO3tLGucOixtq0Em58nVDi3cnm/MO772SerjZmfNqoiz5M7SumOnVEywbHwgHRIplr/gZJDsro3uXBQEuSvNtlJ65glMMUnhiAFCBtjzPXrJbzKKfxxHNef++uMBAAFJC8gEfYrJhPLvOC9xuJSQPFxQAyG+asLiGjrXv2iBLnu/sItSSB3djEvICc7gPCaZwvehA6wUUxT/E8jDJpyXg0WpPtH5p9OimvynNoQzCMDVycbODs7OpfACOtw00bYFc0VmRcB+lME2rOPUHaNll0Bn7IyLSgfXKhNY9RrxofqAAhLA5QKJ21MK3tfQxO8G0j930LvRu9lIYF7h0PQpPSGpZGYkSNYaE++kjKn0FR1BfrWAdVwudcmbyRWj7OfMJzHgsvDSrjPmcZ58qCxkFCMvB35A8Hm/j7UXb4LmT7ieN1yHjLUomckbkXMkdccPQR2fa6zPGsYWQ6RzyyaluyNmaRwx+fODJka4tAIDHY67Ay8/gRDuhSTM2hVZRQ6J3NvvZV82Crzwp9cGPvFZ1MFZJ78P7g3HcRYw4znTgastRhYZE/IjHONhazksraPwepvJVaRPvlXPwlD3MDO6rpsC4nPJmHCFmemiqH/uvUzeFV+BojE5StSKPQ7WTP3m9onhp9+Xxx5TOTj+7dn7Dqvegcz/gxTGRr3qXGWiPBX2lp3XiSkEG4o+QlsEY+sQFIImCyXWTW0w5hCQJgMR";
            //var PubKey = "x8Rb9aIR79hO1w31bFKfetxeT48J4ymBZeHMqKO/OfmOvnRChO3tLGucOixtq0Em58nVDi3cnm/MO772SerjZmfNqoiz5M7SumOnVEywbHwgHRIplr/gZJDsro3uXBQEuSvNtlJ65glMMUnhiAFCBtjzPXrJbzKKfxxHNef++uMBAAE=";


            //C# produced same sigHex .. 720d1ac2aa093f167a5cc2afc28301722443c2b80fc29947c39379c2b967c3a1c28c4bc3b4c2a4c2b32ec387c3964bc28dc3ae76c3abc28bc3b0c29cc3be5c3248c2801076c39ec2bac2aa73c2bb53c284c29bc28ac28ec2a5c2abc2bc5ec3a14c39c3b43d1470c29d47c39741783ec2a1c3b87ec2afc29e4fc298c2adc2b2c2b5c29e0e54c382c3bc50c29e6e745a6cc2a9c2b7c3850c424b71c3a0c2820dc2bbc282c39e10c3b4532a76313914c2b43b2a562e32c394c2b321c2aec2a515
            // Hash as hex" f0e4c2f76c58916ec258f24685 
            //                "msg": "asdf",
            //    "Signature": "577c2a076f6b01c3b6706e43c3847dc2b8c3b51736c2ae3312c3b3c39cc390300901c298c3a27bc2b1c2942cc287c2b94b1fc39a21c3acc2b60bc381c2b945c3a9c2b6c3bb60127dc38bc2a1074cc281c2b8425e13c383c2b1c2adc29c36c3b71f3142c3a0c398c383c2936a4cc3a2c39ec2bbc390c2b213c3adc2a272c3806007c2bac281c3a8c2946f2ec2a7c28f153767c2bd624fc39ac3b67c0dc2b7c3907762c3a5c3a96fc2bfc2b47827c3902b4d3261c389573a557bc3a0c383c2ba",
            //    "Hash": "f0e4c2f76c58916ec258f246851bea091d14d4247a2fc3e18694461b1816e13b"
            //}

            var sigValid =
                        TruRSACrypto.VerifyHash(testMsg.stringifyMsg(),
                        results.Signature,
                        thisWalletKeys.PubKey);



           Assert.IsTrue(sigValid);
        }


        [TestMethod]
        public void RSAKeyTestCompare()
        {
 
            var newRSA =  new WalletKeys() { 
                PubKey = "5ARechqPm5nncc54ApY1Tk9e+lmRzg+OHTNKSVAYwvJSckndtz6SkUAhFibvmGZP9SpHxxswt9JoggIo0lf76K75h/z5ZnkOUTcaMQ361/9vGYQRflB/t7/t5nV2RjimGJXa0QbTXgZxeJwY5iy3IGYmOTmQnc5uIwAjqCIyakEBAAE=", 
                PrivKey = "5ARechqPm5nncc54ApY1Tk9e+lmRzg+OHTNKSVAYwvJSckndtz6SkUAhFibvmGZP9SpHxxswt9JoggIo0lf76K75h/z5ZnkOUTcaMQ361/9vGYQRflB/t7/t5nV2RjimGJXa0QbTXgZxeJwY5iy3IGYmOTmQnc5uIwAjqCIyakEBAAGF7cMDxnIfdTCqehmne07oJlqlNcMcg6p3oE677R3+M5MVqilenpQ8WKyWdFw66a2BVUo/usrMzfuwkVAk7KT7FLrTZepg5o5RzPo9iwrtE9w7xKG62uZOR1lOy70H2oOC8lLJEuq27chnqn+VNKg4NDqC9axvtyPsFF2jvQfD2eSrgm4X0WvvfGWJtMp95I1HA+JX1269QhRpHAdy58K8v2fJQgIinXKjX22g+ye0FxAhzUUK6j3Ay9kYB8cF7yP/ROIbTIdAi+HKomlQlhNIYih8iiGycOeeueLvpMbO5d39QavsQiF4XriqQ4PUoKGxgi+YU0hB31febNCAIGlLxPei4rfrcQCZc1DZhK5KJxMrmlS1wbKN78zWEuh8D3bLnFd0Xyh33J4y/dhzXjC2yPOEv9r2B/lA3Z/gWyOWZzi2NQWbmvDcIu3B52Pf86jBJf2FMrdSHiNFaQWUthaBOzBoxqlWh9kWMjChj0hN555oLerJleQMmpjv0+KjUMGVwJpBhNpCODK4n6pQaHiY03zxg1D3o8pEY22wl/OosLkY7clMVIlBKbuPGP9mpteAJyIo9pOKqWLDhGyHC46E" 
            };

            Console.WriteLine(newRSA);

        }


        [TestMethod]
        public void SignMsgAndVerify()
        {
      
            var stringToEncrypt = "asdfasdfasdfasdfasdfasdf";
 
           // var encryptedBytes = loadWallet.Encrypt(Encoding.ASCII.GetBytes(stringToEncrypt), otherWallet.AddressEncoded);

           // Debug.WriteLine(Encoding.ASCII.GetString(encryptedBytes));

            var hashedEncryptedBytes = TruRSACrypto.Sha1Hash(Encoding.ASCII.GetBytes(stringToEncrypt));

            var loadWalletkeys = EncodeWalletKeys();

            var signedBytes = TruRSACrypto.SignBytes(hashedEncryptedBytes, loadWalletkeys.PrivKey);

            string sigHex = TruRSACrypto.ByteArrayToHexString(signedBytes);

            var sigResult = TruRSACrypto.VerifyHash(stringToEncrypt, sigHex, loadWalletkeys.PubKey);

            Assert.IsTrue(sigResult);

        }


        [TestMethod]
        public void SignMsgAndVerifyFail()
        {

            var stringData = FileOperations.ReadToString("walletInfo.json");
            var keys = JsonConvert.DeserializeObject<WalletKeys>(stringData);
            Console.WriteLine(keys);
     
            var stringToEncrypt = "asdfasdfasdfasdfasdfasdf";

            var loadWalletkeys = EncodeWalletKeys();


            var otherWalletKeys = TruRSACrypto.GenerateNewKeys();


            var encryptedBytes = TruRSACrypto.Encrypt(Encoding.ASCII.GetBytes(stringToEncrypt), otherWalletKeys.PubKey);


            Debug.WriteLine(Encoding.ASCII.GetString(encryptedBytes));

            var hashedEncryptedBytes = TruRSACrypto.Sha1Hash(encryptedBytes);

            var signedBytes = TruRSACrypto.SignBytes(hashedEncryptedBytes, loadWalletkeys.PrivKey);
            string sigHex = TruRSACrypto.ByteArrayToHexString(signedBytes);
            var sigResult = TruRSACrypto.VerifyHash(stringToEncrypt, sigHex, loadWalletkeys.PubKey);

            Assert.IsTrue(!sigResult);

        }

        [TestMethod]
        public void SignMsgAndVerifyFailByAnotherSignature()
        {

            var stringData = FileOperations.ReadToString("walletInfo.json");
            var keys = JsonConvert.DeserializeObject<WalletKeys>(stringData);
            Console.WriteLine(keys);
            var loadWalletkeys = JsonConvert.DeserializeObject<WalletKeys>(stringData);

            var stringToEncrypt = "asdfasdfasdfasdfasdfasdf";
             
            var otherWalletKeys = TruRSACrypto.GenerateNewKeys();
             
            var anotherWalletKeys = TruRSACrypto.GenerateNewKeys();

            var encryptedBytes = TruRSACrypto.Encrypt(Encoding.ASCII.GetBytes(stringToEncrypt), otherWalletKeys.PubKey);
             
            Debug.WriteLine(Encoding.ASCII.GetString(encryptedBytes));

            var hashedEncryptedBytes = TruRSACrypto.Sha1Hash(encryptedBytes);

            var signedBytes = TruRSACrypto.SignBytes(hashedEncryptedBytes, anotherWalletKeys.PrivKey);
             
            string sigHex = TruRSACrypto.ByteArrayToHexString(signedBytes);

            var sigResult = TruRSACrypto.VerifyHash(stringToEncrypt, sigHex, loadWalletkeys.PubKey);

            Assert.IsTrue(!sigResult);

        }


    }

}
