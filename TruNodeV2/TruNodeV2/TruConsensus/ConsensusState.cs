﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruNodeV2.TruConsensus
{
    public enum ConsensusState
    {
        PRE_PREPARE,
        PREPARE,
        COMMIT
    }
}
