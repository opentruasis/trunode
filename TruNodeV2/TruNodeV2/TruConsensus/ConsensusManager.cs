﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TruSharedV2;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{

//psudo code
//    // NEW_ROUND:
//State = NEW_ROUND
//proposer = get_proposers_address(blockchain)
//if (current_validator == proposer )
//    block = create_block(transaction_pool )
//    broadcast_block(block )
//    State = PRE_PREPARED
//// PRE_PREPARED:
//ON message.type == PRE_PREPARE
//    verify_block(message.block )
//    verify_validator(message.block )
//    broadcast_prepare(message.block )
//    State = PREPARED
//// PREPARED:
//ON message.type == PREPARE
//    verify_prepare(message.prepare )
//    verify_validator(message.prepare )
//    prepare_pool.add(message.prepare )
//if (prepare_pool.length > 2F+1 )
//        broadcast_commit(message.prepare )


//    State = COMMITTED
//// COMMITTED:
//ON message.type == COMMIT
//    verify_commit(message.commit )
//    verify_validator(message.commit )
//    commit_pool.add(message.commit )
//if (commit_pool.length > 2F+1 )
//        commit_list = commit_pool.get_commits()
//        block.append(commit_list )
//        blockchain.append(block )

//    State = FINAL_COMMITTED
//// FINAL_COMMITTED:
//new_round()


    public class ConsensusManager
    {

        public ConsensusManager(TruWallet myWallet, TruWalletManager netWallets)
        {
            MyWallet = myWallet;
            NetworkWallets = netWallets;
            LeaderElectionPool = new LeaderElectionPool(MyWallet, NetworkWallets);
            PreparePool = new PreparePool(MyWallet, NetworkWallets);
            CommitPool = new CommitPool(MyWallet, NetworkWallets);
            BlockPool = new BlockPool(MyWallet, NetworkWallets);
            RoundChangePool = new RoundChangePool(MyWallet, NetworkWallets);
        }

        public string SuperNodeLeader = "";

        private TruWallet MyWallet;

        private TruWalletManager NetworkWallets; //Indexed by walletId

        public List<TruTx> ValidationPool = new List<TruTx>();
         
        public bool WorkingOnConsensus { get; set; } = false;

        public DateTime BlockTimeCounter = DateTime.UtcNow;
        public DateTime ElectionTimeCounter = DateTime.UtcNow;

        public int MIN_AGREEMENTS
        {
            get
            {
                var superCount = 0;
                foreach(var super in Program.SuperNodes.Values)
                {
                    if (super.ID != null)
                    {
                        if (Decentralize.Heartbeats[super.ID] > 0)
                        {
                            superCount++;
                        }
                    }
                }
                return 2 * (superCount / 3 ) + 1;  // SuperNodes includes all nodes heartbeating that isnt this node.
            }
        }

        public int ELECTION_MIN
        {
            get
            {
               return Program.SuperNodes.Values.Count + 1;
               
            }
        }

        public PreparePool PreparePool { get; set; }

        public CommitPool CommitPool { get; set; }

        public BlockPool BlockPool { get; set; }

        public RoundChangePool RoundChangePool { get; set; }

        public LeaderElectionPool LeaderElectionPool { get; set; }

        public int ElectionCycle { get; set; } = 0;

        public ConsensusDTO DTO { 
            get
            {
                return new ConsensusDTO()
                {
                    ProposalBlocks = BlockPool.Pool,
                    PrepareBlocks = PreparePool.Pool,
                    CommitBlocks = CommitPool.Pool,
                };
            } 
        }



        public void GrowChain()
        {
            while (true)
            {
                //for now we delete ValidationPool.
                // ValidationPool.Clear();
                Thread.Sleep(1000);
                var now = DateTime.UtcNow;
                List<TruTx> toDelete = new List<TruTx>();
                lock (ValidationPool)
                {
                    foreach (var newTx in ValidationPool)
                    {
                        if (newTx.isVerified)
                        {
                            toDelete.Add(newTx);

                            if (Program.TruLedger.AddNewTx(newTx))
                            {
                                Program.ClientSignalRContext.Clients.All.SendAsync("ChainData", JsonConvert.SerializeObject(new Dictionary<long, TruBlock> { { Program.TruLedger.EndBlockId, Program.TruLedger.Chain[Program.TruLedger.EndBlockId] } }));
                            }
                        }
                        else
                        {
                            var timeSpan = now - DateTime.Parse(newTx.CreatedOn);
                            if (timeSpan.TotalSeconds < 5)
                            {
                                //HbCtx.Clients.Group("validator").SendAsync();
                                Program.ClientSignalRContext.Clients.All.SendAsync("NewTxToValidate", JsonConvert.SerializeObject(newTx));
                            }
                            else
                            {
                                toDelete.Add(newTx);
                            }
                        }
                    }
                }

                foreach (var delTx in toDelete)
                {
                    ValidationPool.Remove(delTx);
                }

                var blockTimeSpan = (now.Ticks -  BlockTimeCounter.Ticks) / 10000000f;
                var electionTime =  (now.Ticks - ElectionTimeCounter.Ticks) / 10000000f;


                if (electionTime > 20 && Program.MySuperNode.ID == SuperNodeLeader || (electionTime > 30)) // Elect a new leader.
                {
                    LeaderElectionPool.Clear();
                    ElectionTimeCounter = now;
                    var leaderNewLeaderVote = LeaderElectionPool.Vote(ElectionCycle, SuperNodeLeader);
                    Decentralize.BroadcastTruConsensusMsg(leaderNewLeaderVote);
                }



                Console.WriteLine("Current Super Node Leader: " + SuperNodeLeader);
                // The leader has to break in here... the only one to.. OR leader lost network so allow anyone to enter after 90 time..
                if (Program.MySuperNode.ID == SuperNodeLeader || (SuperNodeLeader == "" && blockTimeSpan > 30))
                {
        
                    if (blockTimeSpan > 20  &&  Program.SuperNodes.Count > 0) /// Block time..20 seconds.. 
                    {

                        BlockTimeCounter = now;
                        //Leader Election:
                        //List of super Ids...

                        //else if(ElectionState != LeaderElectionState.DONE)
                        //{
                        //    Decentralize.BroadcastTruConsensusMsg(MyVoteForNewLeader);
                        //}


                        //Is there atleast one tx?

                        //if (Program.TruLedger.Chain[Program.TruLedger.EndBlockId].Transactions.Count > 0 && !Program.ConsensusManager.WorkingOnConsensus) // Atleast one Transaction before appending chain..
                        //{
                        //    _ = Task.Run(async () =>
                        //    {
                        //        Program.ConsensusManager.WorkingOnConsensus = true;
                        //        Program.ConsensusManager.BlockTimeCounter = now;
                        //        //Send Broadcast.. of My Chain..

                        //        TruBlockProposal proposal = new TruBlockProposal() { MsgType = ConsensusMsgType.PRE_PREPARE, Block = Program.TruLedger.EndBlock };

                        //        Decentralize.BroadcastTruConsensusMsg(proposal);

                        //    });

                        //}
                    }
                }

            }


        }

    }
}
