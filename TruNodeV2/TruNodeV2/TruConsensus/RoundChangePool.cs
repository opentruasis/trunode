﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{
    public class RoundChangePool
    {

        public RoundChangePool(TruWallet myWallet, TruWalletManager netWallets)
        {
            MyWallet = myWallet;
            NetworkWallets = netWallets;
        }

        public Dictionary<string, List<RoundChangeMsg>> Pool = new Dictionary<string, List<RoundChangeMsg>>();

        private TruWallet MyWallet;

        public TruWalletManager NetworkWallets; //Indexed by walletId


        public RoundChangeMsg ChangeRound(TruBlock block)
        {
            var msg = CreateMsg(block);
            this.Pool[msg.Block.BlockHash] = new List<RoundChangeMsg>();
            this.Pool[msg.Block.BlockHash].Add(msg);
            return msg;
        }


        // creates a round change message for the given block hash
        RoundChangeMsg CreateMsg(TruBlock block)
        {
            RoundChangeMsg roundChange = new RoundChangeMsg() {  Block = block, SignerId = MyWallet.ID };
            TruRSACrypto.SignMsg(roundChange, MyWallet.Keys.PrivKey);
            return roundChange;
        }


        // checks if the message already exists
        public bool ExistingMessage(RoundChangeMsg message)
        {
            return this.Pool[message.Block.BlockHash].FindIndex(p => p.SignerId == message.SignerId) > -1;
        }

        // checks if the message is valid or not
        public bool IsValidMessage(RoundChangeMsg message)
        {
            Console.WriteLine("in valid here");
            // maybe other than just validating the message..
            if (NetworkWallets.ContainsKey(message.SignerId))
            {
                string pubKeyInMyRoledex = NetworkWallets[message.SignerId].Keys.PubKey;
                return message.isValidSig(pubKeyInMyRoledex);
            }
            return false;
        }

        // pushes the message for a block hash into the list
        public void AddMessage(RoundChangeMsg message)
        {
            
            if (this.Pool.ContainsKey(message.Block.BlockHash))
            {
                this.Pool[message.Block.BlockHash].Add(message);
            }
            else
            {
                this.Pool.Add(message.Block.BlockHash, new List<RoundChangeMsg>() { message });
            }
            
        }

        public void Clear()
        {
            Pool = new Dictionary<string, List<RoundChangeMsg>>();
        }
    }
}
