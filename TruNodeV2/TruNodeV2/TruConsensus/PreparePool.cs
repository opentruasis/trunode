﻿using System;
using System.Collections.Generic;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{
    public class PreparePool
    {
        public PreparePool(TruWallet myWallet, TruWalletManager netWallets)
        {
            MyWallet = myWallet;
            NetworkWallets = netWallets;
        }

        public Dictionary<string, List<TruBlockPrepare>> Pool = new Dictionary<string, List<TruBlockPrepare>>();
        // prepare function initialize a list of prepare message for a block
        // and adds the prepare message for the current node and
        // returns it

        private TruWallet MyWallet;

        private TruWalletManager NetworkWallets; //Indexed by walletId


        public TruBlockPrepare Prepare(TruBlock block)
        {
            var prepare = this.CreatePrepare(block);
            this.Pool.Add(block.BlockHash, new List<TruBlockPrepare>());
            this.Pool[block.BlockHash].Add(prepare);
            return prepare;
        }

        // creates a prepare message for the given block
        TruBlockPrepare CreatePrepare(TruBlock block)
        {
            TruBlockPrepare prepare = new TruBlockPrepare() { Block = block, SignerId = MyWallet.ID };
            TruRSACrypto.SignMsg(prepare, MyWallet.Keys.PrivKey);
            return prepare;
        }

        // pushes the prepare message for a block hash into the list
        public void AddPrepare(TruBlockPrepare prepare)
        {
            if (this.Pool.ContainsKey(prepare.Block.BlockHash))
            {
                this.Pool[prepare.Block.BlockHash].Add(prepare);
            }
            else
            {
                this.Pool.Add(prepare.Block.BlockHash, new List<TruBlockPrepare>() { prepare });
            }
        }

        // checks if the prepare message already exists
        public bool ExistingPrepare(TruBlockPrepare prepare)
        {
            return this.Pool[prepare.Block.BlockHash].FindIndex(p => p.SignerId == prepare.SignerId) > -1;
        }

    // checks if the prepare message is valid or not
        public bool IsValidPrepare(TruBlockPrepare prepare)
        {
            if (Program.NetworkWallets.ContainsKey(prepare.SignerId))
            {
                return prepare.isValidSig(NetworkWallets[prepare.SignerId].Keys.PubKey);
            }
            return false;
        }

        public void Clear()
        {
            Pool = new Dictionary<string, List<TruBlockPrepare>>();
        }
    }
}