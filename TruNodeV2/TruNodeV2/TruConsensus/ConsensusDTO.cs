﻿using System.Collections.Generic;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{
    public class ConsensusDTO
    {
        //Data  we want to check while debugging Consensus.
        public List<TruBlock> ProposalBlocks { get; set; }
        
        public Dictionary<string, List<TruBlockPrepare>> PrepareBlocks { get; set; }
 
        public Dictionary<string, List<TruBlockCommit>> CommitBlocks { get; set; }

        public TruLedger CurrentLedger { get; set; }
 
    }
}