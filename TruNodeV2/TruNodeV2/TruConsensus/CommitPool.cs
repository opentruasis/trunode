﻿using System;
using System.Collections.Generic;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{
    public class CommitPool
    {
        public CommitPool(TruWallet myWallet, TruWalletManager netWallets)
        {
            MyWallet = myWallet;
            NetworkWallets = netWallets;
        }
        public Dictionary<string, List<TruBlockCommit>> Pool = new Dictionary<string, List<TruBlockCommit>>();

        private TruWallet MyWallet;

        public TruWalletManager NetworkWallets; //Indexed by walletId


        public TruBlockCommit Commit(TruBlock block)
        {
            var commitMsg = this.CreateCommit(block);
            this.Pool[commitMsg.Block.BlockHash] = new List<TruBlockCommit>() { commitMsg };
  
            return commitMsg;
           // return null;
        }

        // creates a commit message for the given prepare message
        TruBlockCommit CreateCommit(TruBlock block)
        {
            var commit = new TruBlockCommit() { Block = block, SignerId = MyWallet.ID };
            TruRSACrypto.SignMsg(commit, MyWallet.Keys.PrivKey);
            return commit;
        }

        // checks if the commit message already exists
        public bool ExistingCommit(TruBlockCommit commit)
        {
            return this.Pool[commit.Block.BlockHash].FindIndex(p => p.SignerId == commit.SignerId) > -1;
        }
         

        // checks if the commit message is
        // or not
        public bool IsValidCommit(TruBlockCommit commit)
        {
            if (NetworkWallets.ContainsKey(commit.SignerId))
            {
                return true;
            }
           
            return false;
        }



        // pushes the commit message for a block hash into the list
        public void AddCommit(TruBlockCommit commit)
        {
            if (this.Pool.ContainsKey(commit.Block.BlockHash))
            {
                this.Pool[commit.Block.BlockHash].Add(commit);
            }
            else
            {
                this.Pool.Add(commit.Block.BlockHash, new List<TruBlockCommit>() { commit });
            }
        }

        public void Clear()
        {
            Pool = new Dictionary<string, List<TruBlockCommit>>();
        }
    }

}