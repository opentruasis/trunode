﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{
    public class BlockPool
    {

        public List<TruBlock> Pool = new List<TruBlock>();

        public BlockPool(TruWallet myWallet, TruWalletManager netWallets)
        {
            MyWallet = myWallet;
            NetworkWallets = netWallets;
        }
 
        private TruWallet MyWallet;

        public TruWalletManager NetworkWallets; //Indexed by walletId
 
        // pushes block to the chain
        public void AddBlock(TruBlock block)
        {
            if (!this.Pool.Contains(block))
            {
                this.Pool.Add(block);
            }
        }

        // returns the blcok for the given hash
        TruBlock GetBlock(string hash)
        {
            return this.Pool.Find(b => b.BlockHash == hash);
        }

        public bool ExisitingBlock(TruBlock block)
        {
            return this.Pool.Contains(block);
        }
        
        public TruBlock GetHeaviestValidBlock()
        {
            TruBlock heaviestBlock = null;
            int heavestWeight = 0;
            foreach(TruBlock block in this.Pool)
            {
                if (Program.TruLedger.IsValidBlock(block)) {

                    if (block.BlockWeight > heavestWeight)
                    {
                        heavestWeight = block.BlockWeight;
                        heaviestBlock = block;
                    }
                }
            }

            if(heaviestBlock == null)
            {
                return Program.TruLedger.EndBlock;
            }

            return heaviestBlock;
        }

        public void Clear()
        {
            Pool = new List<TruBlock>();
        }
    }
}
