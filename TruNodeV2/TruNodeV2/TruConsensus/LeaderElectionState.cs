﻿namespace TruNodeV2.TruConsensus
{
    public enum LeaderElectionState
    {
        DONE,
        START,
        COUNTING
    }
}