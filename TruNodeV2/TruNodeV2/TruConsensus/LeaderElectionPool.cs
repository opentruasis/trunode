﻿using System;
using System.Collections.Generic;
using System.Linq;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.TruConsensus
{
    public class LeaderElectionPool
    {
        private TruWallet myWallet;
        private TruWalletManager networkWallets;

        Random rand = new Random();
 
        public LeaderElectionPool(TruWallet myWallet, TruWalletManager networkWallets)
        {
            this.myWallet = myWallet;
            this.networkWallets = networkWallets;
        }

        public Dictionary<string, TruSuperVote> Pool = new Dictionary<string, TruSuperVote>();

        public TruSuperVote Vote(int electionCycle, string lastLeader)
        {
            var vote = new TruSuperVote();
            vote.SignerId = myWallet.ID;
            vote.Sequence = new Random().Next();
            vote.ElectionCycle = electionCycle;
            TruRSACrypto.SignMsg(vote, myWallet.Keys.PrivKey);
            this.AddVote(vote);
            return vote;
        }


        // pushes the prepare message for a block hash into the list
        public void AddVote(TruSuperVote vote)
        {
            if (networkWallets.ContainsKey(vote.SignerId))
            {
                if (vote.isValidSig(networkWallets[vote.SignerId].Keys.PubKey))
                {
                    if (!this.Pool.ContainsKey(vote.SignerId))
                    {              
                        this.Pool.Add(vote.SignerId, vote);
                    }
                }
            }
        }
 
        public TruLeaderChosen GetNewLeader()
        {
            var winningSequence = 0;
            string victor = "";
            lock (Pool)
            {
                foreach (var (signerId, vote) in Pool)
                {
                    if (vote.Sequence >= winningSequence)
                    {
                        winningSequence = vote.Sequence;
                        victor = vote.SignerId;
                    }
                }
            }
            if(winningSequence == 0)
            {
                return null;
            }
 
            return new TruLeaderChosen(victor, winningSequence);
        }


        public void Clear()
        {
            Pool = new Dictionary<string,  TruSuperVote>();
        }
    }

}