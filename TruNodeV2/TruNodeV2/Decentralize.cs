﻿using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TruNodeV2.Hubs;
using TruNodeV2.TruConsensus;
using TruSharedV2;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2
{
    public static class Decentralize  
    {
  
        public static Dictionary<string, int> Heartbeats = new Dictionary<string, int>();
        public static Dictionary<string, int> LastHeartbeats = new Dictionary<string, int>();

        public static Dictionary<string, BlockConsensus> CloseOutBlocks = new Dictionary<string, BlockConsensus>();

        public static BlockConsensus WinningBlock = null;

        public static async Task GetConnected(string url)
        {
            Console.WriteLine("Get Connected");
            var connectUrl = url + "/ws/super";
            Console.WriteLine(connectUrl);
            HubConnection superHubClientConnection = new HubConnectionBuilder()  /////CLIENT SIDE!!!!!!!!
                .WithUrl(new Uri(connectUrl))
               // .WithAutomaticReconnect()
                .Build();

            Debug.WriteLine("Connection...");
            Debug.WriteLine(superHubClientConnection);

            superHubClientConnection.Closed += async (error) =>
            {
                //remove from list of available super nodes... 
                var superForRemoval = Program.SuperNodes.Values.FirstOrDefault(s => s.SuperURL == url);
                lock (Program.SuperNodes)
                {
                    Program.SuperNodes.Remove(superForRemoval.ID);
                }
            };

            superHubClientConnection.On("Disconneting", async () =>
            {
                await superHubClientConnection.StopAsync();
            });


            superHubClientConnection.On<string>("VersionAck", async (msg) =>
            {
                var versionAck = JsonConvert.DeserializeObject<TruVersionAck>(msg);

                if (Program.SuperNodes.ContainsKey(versionAck.SuperNodeInfo.ID))
                {
                    Program.SuperNodes[versionAck.SuperNodeInfo.ID] = versionAck.SuperNodeInfo;
                    Program.SuperNodes[versionAck.SuperNodeInfo.ID].SoccetConnectionID = superHubClientConnection.ConnectionId;
                    Program.SuperNodes[versionAck.SuperNodeInfo.ID].WebSoccket = superHubClientConnection;
                }
                else
                {
                    versionAck.SuperNodeInfo.SoccetConnectionID = superHubClientConnection.ConnectionId;
                    versionAck.SuperNodeInfo.WebSoccket = superHubClientConnection;
                    Program.SuperNodes.Add(versionAck.SuperNodeInfo.ID, versionAck.SuperNodeInfo);
                }

            });

            superHubClientConnection.On<string>("HeartbeatResponse", async (msg) =>
            {
               // Console.WriteLine("Heatbeat Response....");
               // Console.WriteLine(heartbeatResp);heartBeatResp
                var heartbeatRespData = JsonConvert.DeserializeObject<HeartBeatRespDTO>(msg);
                Console.WriteLine("HEartbeat Response Data: " + heartbeatRespData.heartbeat+  " -- from : " + heartbeatRespData.walletId);
                if (heartbeatRespData.heartbeat == Heartbeats[heartbeatRespData.walletId])
                {
                    Heartbeats[heartbeatRespData.walletId] += 1;
                }
                else
                {
                    Heartbeats[heartbeatRespData.walletId] = 0;
                    await superHubClientConnection.StopAsync();
                }

            });


            superHubClientConnection.On<string>("SuperUrlList", async (msg) =>
            {
                var listOfSuperUrls = JsonConvert.DeserializeObject<List<string>>(msg);
  
                foreach(var url in listOfSuperUrls)
                {
                    Program.SuperUrls.Add(url);
                }
            });


            superHubClientConnection.On<string>("ChainData", async (ChainData) =>
            {
                Console.WriteLine("Chain Data.......");
                Console.WriteLine(ChainData);
                var chain = JsonConvert.DeserializeObject<SortedDictionary<long, TruBlock>>(ChainData);
                Console.WriteLine(chain);

                foreach (var (blockId, block) in chain)
                {
                    Console.WriteLine("Block Id: " + blockId);
                    Console.WriteLine(block);
                    if (Program.TruLedger.Chain.ContainsKey(blockId))
                    {
                        Program.TruLedger.Chain[blockId] = block;
                    }
                    else
                    {
                        Program.TruLedger.Chain.Add(blockId, block);
                    }
                }

                if (Program.TruLedger.ValidChain)
                {
                    Console.WriteLine("- ");
                    Console.WriteLine("- ");
                    Console.WriteLine("- ");
                    Console.WriteLine("Valid Chain");
                    Program.TruLedger.PersistAll();
                }
                else
                {
                    Console.WriteLine("Invalid Chain.. ;(  Try and repair");
                }
       
                Console.WriteLine("Tru Ledger chain..: " + Program.TruLedger.Chain.Count);
            });

            superHubClientConnection.On<string>("NetworkWallets", async (netWallets) =>
            {
                Console.WriteLine("Net Wallets Data.......");
                Console.WriteLine(netWallets);
                var wallets = JsonConvert.DeserializeObject<Dictionary<string, TruWallet>>(netWallets);
                Program.NetworkWallets.Append(wallets);
            });

            superHubClientConnection.On<string>("GetNetworkWallet", async (netWallet) =>
            {
                Console.WriteLine("Get Wallet Request.................");
                if (Program.NetworkWallets.ContainsKey(netWallet))
                {
                    Console.WriteLine("SENDING WALLET REQUEST>>> " + netWallet);
                   await superHubClientConnection.SendAsync("WalletDataUpdate", JsonConvert.SerializeObject(Program.NetworkWallets[netWallet]));
                }
            });

        
            try
            {
                await superHubClientConnection.StartAsync();
                //var superOnNet = new SuperNodeInfo()
                //{
                //    SuperURL = url,
                //    SoccetConnectionID = superHubClientConnection.ConnectionId,
                //    WebSoccket = superHubClientConnection
                //};
 
                Program.SuperUrls.Add(url);

                var truVersion = new TruVersion()
                {
                    Version = Program.Version,
                    connectionId = superHubClientConnection.ConnectionId,
                    SuperNodeInfo = Program.MySuperNode
                };


                superHubClientConnection.SendAsync("Version", JsonConvert.SerializeObject(truVersion));

            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to connect..");
                Console.WriteLine(e.Message);
                //Maybe remove this SuperNode
                lock (Program.SuperUrls._lockObj)
                {
                    if (url != Program.SeedSuperNodeURL)
                    {
                        Program.SuperUrls.Remove(url);
                    }
                }
            }
             
        }

        public static void ResetConsensusManager()
        {
            Program.ConsensusManager.BlockPool.Clear();
            Program.ConsensusManager.PreparePool.Clear();
            Program.ConsensusManager.CommitPool.Clear();
            Program.ConsensusManager.RoundChangePool.Clear();
        }

        public static async void BroadcastTruConsensusMsg(TruConsensusMsg msg)
        {
            lock (Program.SuperNodes)
            {
                foreach (var (id, info) in Program.SuperNodes)
                {
                    if (info.WebSoccket == null)
                    {
                        Decentralize.GetConnected(info.SuperURL);
                    }
                    else
                    {
                        info.WebSoccket.InvokeAsync("TruConsensusMsg", JsonConvert.SerializeObject(msg));
                    }
                }
            }
        }


        public static async void SendSuperHeartbeat()
        {
            Thread.Sleep(5000);

            while (true)
            {

                Program.CheckOnLocalClients();

                Thread.Sleep(5000);
         
                var supers = Program.SuperNodes.Values.ToList();

                var superUrlList = Program.SuperUrls.URLS.ToArray();

                var connectedUrls = new List<string>();
                supers.ForEach(s =>
                {
                    connectedUrls.Add(s.SuperURL);
                });

                foreach (var url in superUrlList)  // Check to make sure the node is connected to all the Super url list.
                {
                    if (!connectedUrls.Contains(url) && url != Program.MyURL)
                    {
                        Decentralize.GetConnected(url);
                    }
                }

                //list of supers to remove if HB is expired.

                List<string> supersToDelete = new List<string>();

                foreach (var super in supers)
                {

                    if (super.WebSoccket == null)
                    {
                        Decentralize.GetConnected(super.SuperURL);
                    }
                    else
                    {

                    
                        var SuperHeartBeat = new SuperHeartbeatDTO();
                        SuperHeartBeat.SuperUrlListCount = Program.SuperUrls.Count;
                        SuperHeartBeat.SuperURL = Program.MyURL;
                        int heartBeat = 0;
                        int lastHeartBeat = 0;
                        if (super.ID != null)
                        {
                            if (Heartbeats.ContainsKey(super.ID))
                            {
                                heartBeat = Heartbeats[super.ID];
                            }
                            else
                            {
                                Heartbeats.Add(super.ID, 0);
                            }
                       
                            if (LastHeartbeats.ContainsKey(super.ID))
                            {
                                lastHeartBeat = LastHeartbeats[super.ID];
                            }
                            else
                            {
                                LastHeartbeats.Add(super.ID, 0);
                            }

                            if (lastHeartBeat == heartBeat)
                            {
                                //Disconnect
                                supersToDelete.Add(super.ID);

                            }
                        }

                        SuperHeartBeat.heartbeat = heartBeat;
                        SuperHeartBeat.connectionId = super.SoccetConnectionID;
                        SuperHeartBeat.connectedFullNodeCount = Program.GetConnectedFullNodeClients();
                        SuperHeartBeat.endblockID = Program.TruLedger.EndBlockId;
                        SuperHeartBeat.currentHash = Program.TruLedger.GetCurrentBlockHash();
                        SuperHeartBeat.currentSuperLeader = Program.ConsensusManager.SuperNodeLeader;
                        Program.MySuperNode.WalletCount = Program.NetworkWallets.Count;
                        SuperHeartBeat.superNodeInfo = Program.MySuperNode;
                        Console.WriteLine("Sending My Heartbeat " + heartBeat);
                        super.WebSoccket.InvokeAsync("HeartBeat", JsonConvert.SerializeObject(SuperHeartBeat));
                    }

                }


                foreach(var superId in supersToDelete)
                {
                    if(superId == Program.ConsensusManager.SuperNodeLeader)
                    {
                        Program.ConsensusManager.LeaderElectionPool.Clear();
                        //Do a new vote..
                        var leaderNewLeaderVote = Program.ConsensusManager.LeaderElectionPool.Vote(Program.ConsensusManager.ElectionCycle, Program.ConsensusManager.SuperNodeLeader);
                        Decentralize.BroadcastTruConsensusMsg(leaderNewLeaderVote);
                        var now = DateTime.UtcNow;
                        Program.ConsensusManager.ElectionTimeCounter = now;
                    }
                    Program.SuperNodes.Remove(superId);
                }

                 
            }
        }


        public static async Task BroadcastLatestBlock()
        {
            foreach (var (id, info) in Program.SuperNodes)
            {
  
                Debug.WriteLine("Broadcasting Last Block.." + Program.TruLedger.EndBlockId  );

                var closeoutBlock = new BlockConsensus()
                {
                    SuperURL = info.SuperURL,
                    SignerId = Program.MySuperNode.ID,
                    LatestBlock = Program.TruLedger.Chain[Program.TruLedger.EndBlockId]
                };

                TruRSACrypto.SignMsg(closeoutBlock, Program.MyWallet.Keys.PrivKey);
                         
                info.WebSoccket.InvokeAsync("CloseoutBlock", JsonConvert.SerializeObject(closeoutBlock));
       
            }
        }


        public static async Task<BlockConsensus> GetHeaviestBlock()
        {
         
            BlockConsensus HeaviestLastBlock = new BlockConsensus()
            {
                LatestBlock = Program.TruLedger.Chain[Program.TruLedger.EndBlockId],
                SuperURL = Program.MyURL
            };// initialize as My view of the last block..
            
            foreach(var (superUrl, blockConsensus) in CloseOutBlocks)
            {
                if(blockConsensus.LatestBlock.BlockWeight > HeaviestLastBlock.LatestBlock.BlockWeight)
                {
                    HeaviestLastBlock = blockConsensus;
                }
            }

            CloseOutBlocks.Clear();
      
            return HeaviestLastBlock;
 
        }

        public static async Task BroadcastWinningBlock(BlockConsensus winningBlock)
        {
 
            foreach (var (id, info) in Program.SuperNodes)
            {
                Debug.WriteLine("Broadcasting Last Block.." + Program.TruLedger.EndBlockId);
                info.WebSoccket.InvokeAsync("WinningBlock", JsonConvert.SerializeObject(winningBlock));
            }
 
        }


        public static async Task<BlockConsensus> ConsensusAchieved(BlockConsensus heaviestBlock)
        { 
            var localWinningBlocks = heaviestBlock;
            //then see if winning block consensus is good..

            TruBlock MyBelivedConsensusBlock = localWinningBlocks.LatestBlock;  // initialize as My view of the last block..
             
            Dictionary<string, int> hashDict = new Dictionary<string, int>();
             
            string winningHash = localWinningBlocks.LatestBlock.BlockHash;

            string myBelievedWinnerHash = MyBelivedConsensusBlock.BlockHash;
             
            if (myBelievedWinnerHash == winningHash)
            {
                Console.WriteLine("NICE!!");
                return WinningBlock;
            }

            return null;
            
        }

    }
}
