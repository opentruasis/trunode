using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TruV2Shared;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System.Diagnostics;
using TruNodeV2.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Hosting.Server;
using TruV2Shared.TruDataUnits;
using System.Net.Http;
using System.Net;
using System.Net.Sockets;
using TruNodeV2.TruConsensus;
using TruSharedV2;

namespace TruNodeV2
{
    public class Program
    {
        public static string Version => "2.0.1";
         
        public static TruWalletManager NetworkWallets = new TruWalletManager(0); //Indexed by walletId
 
        public static TruLedger TruLedger = null;

        public static string TruasisWalletId = "";

        public static TruWallet MyWallet = null;

        public static SuperNodeInfo MySuperNode = null;

        public static Dictionary<string, HeartbeatDTO> ConnectedWallets = new Dictionary<string, HeartbeatDTO>();

        public static Dictionary<string, SuperNodeInfo> SuperNodes = new Dictionary<string, SuperNodeInfo>();

        public static Dictionary<string, TruApp> TruApps = new Dictionary<string, TruApp>();

        public static Dictionary<string, TruToken> TruTokens = new Dictionary<string, TruToken>();

        public static string SeedSuperNodeURL { get; set; } = "https://localhost:44380";  //URL For initial connect.

        public static SuperUrlManager SuperUrls { get; set; } 

        public static string MyURL { get; set; } = "";

        public static ConsensusManager ConsensusManager { get; set; }

        public static void Main(string[] args)
        {
            //TruNode Starts Up..

            //Check if TruasisWallet.json exists..
            //Determine
            //Initialize TruLedger.

            //Debug.WriteLine("Char Codes: ");
            //Debug.WriteLine((char)143);
            //char charh =  Convert.ToChar(122);
            //char charv = (char)143;

            //Debug.WriteLine(charv); 

            //Debug.WriteLine(charh);

            //Debug.WriteLine("------");

            //char[] characters = System.Text.Encoding.ASCII.GetChars(new byte[] { 143 });
            //char c = characters[0];

            //Debug.WriteLine(c);

            //Debug.WriteLine("------");

            //Connect to another Super Node..


            //To run you need the --urls arg
            bool hasUrlsTag = false;


            foreach (var arg in args)
            {
                var argSplit = arg.Split('=');
                var argKey = "";
                if(argSplit.Length > 1)
                {
                    argKey = argSplit[0];
                }
                else
                {
                    argKey = arg;
                }

                if(argKey == "--urls")
                {
                    hasUrlsTag = true;
                    MyURL = argSplit[1];
                }
            }

            Console.WriteLine("My URL --- ");
            Console.WriteLine(MyURL);
            if (!hasUrlsTag)
            {
                Environment.Exit(-1);
            }
            //Determine if the URL is valid..
            try
            {
                Uri myUri = new Uri(MyURL);
                var ip = Dns.GetHostAddresses(myUri.Host)[0];

                if (myUri.Host != "localhost" && myUri.Host != "127.0.0.1")
                {
                    if (!IsLocalIPAddress(ip.Address.ToString()) && ip.Address.ToString() != "127.0.0.1")
                    {
                        throw new Exception("Bad URL: Make sure you are supplying only one URL is mapped to an IP on your machine");
                    }
                }
                Console.WriteLine("My URL Scheme " + myUri.Scheme);

#if (RELEASE)
                if (myUri.Scheme != "https")
                {
                    throw new Exception("Make sure your URL is of Scheme HTTPS");
                }
#endif

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(-1);
            }
             
            if ( NetworkWallets.Count < 1 && MyURL == SeedSuperNodeURL )
            {
                var jsonTruasisWallet = FileOperations.ReadToString("SuperNode.json");
                TruWallet TruasisWallet = JsonConvert.DeserializeObject<TruWallet>(jsonTruasisWallet);
                if (TruasisWallet == null)
                {
                    TruasisWallet = new TruWallet();
                    TruasisWallet.FullName = "Truasis Network";
                    TruasisWallet.Handle = "truasis";
                }
                //write out Wallet..
                TruasisWallet.Keys.Private = false;
                FileOperations.WriteStringToFile(JsonConvert.SerializeObject(TruasisWallet), "SuperNode.json");

                TruasisWalletId = TruasisWallet.ID;

                MyWallet = TruasisWallet;
                NetworkWallets.Add(MyWallet.ID, MyWallet);
                TruLedger = new TruLedger(TruasisWallet, NetworkWallets);
                MySuperNode = new SuperNodeInfo(){ ID = MyWallet.ID, SuperURL = MyURL, PubKey = MyWallet.Keys.PubKey };
 
 
            }
            else
            {
                var jsonTruasisWallet = FileOperations.ReadToString("SuperNode.json");
                Console.WriteLine("String Wallet: ");
                Console.WriteLine(jsonTruasisWallet);
                TruWallet TruasisWallet = JsonConvert.DeserializeObject<TruWallet>(jsonTruasisWallet);
                if(TruasisWallet == null)
                {
                    Console.WriteLine("To run a Super Node please put your SuperNode.json file in this working directory");
                    Environment.Exit(-1);
                }
                TruasisWalletId = TruasisWallet.ID;
                MyWallet = TruasisWallet;
                NetworkWallets.Add(MyWallet.ID, MyWallet);
                TruLedger = new TruLedger(NetworkWallets);
                MySuperNode = new SuperNodeInfo() { ID = MyWallet.ID, SuperURL = MyURL, PubKey = MyWallet.Keys.PubKey };
  
            }

            SuperUrls = new SuperUrlManager(SeedSuperNodeURL);

            TruLedger.LoadChain();

            ConsensusManager = new ConsensusManager(MyWallet, NetworkWallets);
            bool seedConnected = false;
            foreach (var url in SuperUrls.URLS)
            {
                if (MyURL != url)
                {
                    if(url == SeedSuperNodeURL)
                    {
                        seedConnected = true;
                    }
                    Decentralize.GetConnected(url);
                    Thread.Sleep(10);
                }
            }
 
         
           
            Task.Run(() =>
            {
                ConsensusManager.GrowChain();
            });

            Task.Run(() =>
            {
                Decentralize.SendSuperHeartbeat();
            });
            
            var host = CreateHostBuilder(args).Build();
            ClientSignalRContext = (IHubContext<TruClientHub>) host.Services.GetService(typeof(IHubContext<TruClientHub>));
            host.Run();
        }


         
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://*:5001");
                });


        private static bool IsLocalIPAddress(string myIp)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            Console.WriteLine("Host Address List: ");
            Console.WriteLine(host.AddressList);
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (ip.Address.ToString() == myIp)
                    {
                        Console.WriteLine("IP Adress Is Good!");
                        return true;
                    }
                }
            }
            return false;
        }

 
        public static int GetConnectedFullNodeClients()
        {
            int fullNodesConnectedCount = 0;
            Program.ConnectedWallets.Values.ToList().ForEach((hbdto) => { 
                if(hbdto.endblockID == TruLedger.EndBlockId)
                {
                        fullNodesConnectedCount++;
                }
            });

            return fullNodesConnectedCount;
        }

        public static void CheckOnLocalClients()
        {
            List<string> connectionsForRemoval = new List<string>();
            foreach(var ( connectionId , heartBeatDTO) in ConnectedWallets)
            {
                var now = DateTime.UtcNow;
                var timeSpan = now - heartBeatDTO.hbtime;
                if (timeSpan.TotalSeconds > 5)
                {
                    connectionsForRemoval.Add(connectionId);
                }
            }

            foreach(var connId in connectionsForRemoval)
            {
                ConnectedWallets.Remove(connId);
            }
        }

      

 
        public static IHubContext<TruClientHub> ClientSignalRContext { get; internal set; } = null;
      


    }


      
 
}
