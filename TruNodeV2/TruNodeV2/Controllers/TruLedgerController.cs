﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TruNodeV2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruLedgerController : Controller
    {
        // GET: api/<TruLedgerController>
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            Console.WriteLine("HELLO");
            return Content(JsonConvert.SerializeObject( Program.TruLedger.Chain ) );
        }

        // GET api/<TruLedgerController>/5
        [HttpGet("blockCount")]
        public  ActionResult  GetBlockCount()
        {
            return Content(Program.TruLedger.Chain.Count().ToString());
        }

        [HttpGet("size")]
        public  ActionResult  GetLedgerSize()
        {
            double size = Program.TruLedger.LedgerSize;
            string ledgerSize = "";
            if (size / 1024 > 6000){
                size = size / 1024 / 1024;
                ledgerSize = Math.Round(size, 4) + " GB";
            }
            else
            {
                size = size / 1024;
                ledgerSize = Math.Round( size, 4) + " MB";
            }
            return Content(ledgerSize);
        }

        // GET api/<TruLedgerController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(ulong id)
        {
            //return Content(JsonConvert.SerializeObject(Program.TruLedger.TxAt(id)));
            return Content("");
        }

   


      


        // POST api/<TruLedgerController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] string value)
        {
            return Content("");
        }

        // PUT api/<TruLedgerController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TruLedgerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
