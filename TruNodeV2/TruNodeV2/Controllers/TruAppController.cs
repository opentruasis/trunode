﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TruNodeV2.Controllers
{

    public class NewTruApp
    {
         
        public string ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<IFormFile> ShowcaseFiles { get; set; } // Files

        public IFormFile Program { get; set; } //File Names

        public string GitRepoURL { get; set; }

        public string Hash { get; set; }

        public string CreatedBy { get; set; }
   
    }


    [Route("api/[controller]")]
    [ApiController]
    public class TruAppController : Controller
    {
        // GET: api/<ValuesController>
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Content(JsonConvert.SerializeObject(Program.TruApps)); 
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id)
        {
            if (Program.TruApps.ContainsKey(id))
            {

                //read in file..serve as base64 string.
                var programFile = Program.TruApps[id].Program;

                byte[] fileBytes = System.IO.File.ReadAllBytes("wwwroot/apps/" + programFile);

                //bytes t0 base 64

                var fileBase64 = Convert.ToBase64String(fileBytes);


                return Content(fileBase64);
            }
            return NotFound();
        }

      

        [HttpPost]
        public async Task<ActionResult> Post([FromForm] NewTruApp app)
        {
            var newApp = new TruApp();

            var fileNameSplit = app.Program.FileName.Split(".");
            newApp.Program = newApp.ID + "." + fileNameSplit[1];

           
            string appPath = "wwwroot/apps";


            if (!Directory.Exists(appPath))
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(appPath);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(appPath));
            }

            string path = Path.Combine(appPath+"/", newApp.Program);

            using (Stream stream = new FileStream(path, FileMode.Create))
            {
                app.Program.CopyTo(stream);

            }

            var programBytes = System.IO.File.ReadAllBytes(path);
            

           var hashCalc = TruRSACrypto.Sha1Hash(Convert.ToBase64String(programBytes));

            if (hashCalc == app.Hash.ToUpper())
            {
                newApp.Hash = hashCalc;
                //TODO... Save program to disk an
                

                foreach(var file in app.ShowcaseFiles)
                {

                    var showcaseFileNameSplit = file.FileName.Split(".");
                    var showCaseFileName = Guid.NewGuid().ToString() + "." + showcaseFileNameSplit[1];
                    var showcasePath = Path.Combine(appPath + "/", showCaseFileName);

                    newApp.ShowcaseFiles.Add(showCaseFileName);

                    using (Stream stream = new FileStream(showcasePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                newApp.CreatedBy = app.CreatedBy;
                newApp.Name = app.Name;
                newApp.GitRepoURL = app.GitRepoURL;
                newApp.Description = app.Description;


                Program.TruApps.Add(newApp.ID, newApp);
                return Content(JsonConvert.SerializeObject(newApp));
            }
            else
            {

                //remove file from path..


            }

               
           
            return NotFound();

        }

            // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] TruApp data)
        {

            return Ok();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            if (Program.TruApps.ContainsKey(id))
            {
                Program.TruApps.Remove(id);
            }
            return Ok();
        }
    }
}
