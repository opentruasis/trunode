﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TruNodeV2.Hubs;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TruWalletController : Controller
    {
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Content(JsonConvert.SerializeObject(Program.NetworkWallets));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(string id)
        {
            string urlsString = "";
            if (Request.IsHttps)
            {
                urlsString = "https://" + Request.Host.Value;
            }
            else
            {
                urlsString = "http://" + Request.Host.Value;
            }

            if (Program.MyURL != urlsString)
            {
                Console.WriteLine("Fatal useage of program. URL missmatch");
                Response.StatusCode = 400;
                _ = Task.Run(() =>
                {
                    Thread.Sleep(1000);
                    Environment.Exit(-1);
                });
                return Content(JsonConvert.SerializeObject(new Dictionary<string, string>(){{"error","Super Node is Faulty. Shtting Down."}}));
            }

            if (Program.NetworkWallets.ContainsKey(id))
            {
                //Calculate Latest  balance..

                var latestBalances = Program.TruLedger.Balances;

                Console.WriteLine("MMM NETWORK WALLET CONTAINS THIS GUY>>" + id);

                var wallet = Program.NetworkWallets[id];
                if (latestBalances.ContainsKey(id))
                {
                    wallet.Balance = latestBalances[id];
                }
                else
                {
                    wallet.Balance = 0;
                }

                return Content(JsonConvert.SerializeObject(wallet));
            }


            Response.StatusCode = 400;
            return Content("Wallet Could Not Be found");
        }



        [HttpPost]
        public async Task<ActionResult> Post([FromBody]  WalletDataDTO newWallet)
        {
            Console.WriteLine(newWallet);
   
            var wallet = Program.NetworkWallets.FindByPubKey(newWallet.Keys["PubKey"]);
 
            if (wallet == null) {
                var walletJoining = new TruWallet();
                walletJoining.PushNotifToken = newWallet.PushNotifToken;
                walletJoining.ProfilePicBase64 = newWallet.ProfilePicBase64;
                walletJoining.Handle = newWallet.Handle;
                walletJoining.FullName = newWallet.FullName;
                walletJoining.Keys = new WalletKeys() { PubKey = newWallet.Keys["PubKey"] };
 
                Program.NetworkWallets.Add(walletJoining.ID, walletJoining);

               _ = Task.Run(async () => { 
                    foreach (var (url, superInfo) in Program.SuperNodes)
                    {
                        if (superInfo.WebSoccket != null)
                        {
                            superInfo.WebSoccket.InvokeAsync("NewWalletJoined", JsonConvert.SerializeObject(walletJoining));
                        }
               
                    }
                });

                return Content(JsonConvert.SerializeObject(walletJoining));
            }

            Response.StatusCode = 400;
            return Content("Wallet already exists");
        }

        [HttpPost("search")]
        public async Task<ActionResult> Search([FromBody] Dictionary<string,string> walletSearchStr)
        {
            var searchString = walletSearchStr["searchString"];
            List<TruWallet> filtered = Program.NetworkWallets.Search(searchString);
  
            return Content(JsonConvert.SerializeObject(filtered));
        }



        [HttpPut]
        public async Task<ActionResult> Put([FromBody] WalletDataDTO walletUpdate)
        {
            Console.WriteLine(walletUpdate);
         
            if (Program.NetworkWallets.ContainsKey(walletUpdate.ID))
            {
                var walletInDb = Program.NetworkWallets[walletUpdate.ID];

                walletInDb.PushNotifToken = walletUpdate.PushNotifToken;
                walletInDb.ProfilePicBase64 = walletUpdate.ProfilePicBase64;
                walletInDb.Handle = walletUpdate.Handle;
                walletInDb.FullName = walletUpdate.FullName;
                walletInDb.Keys = new WalletKeys() { PubKey = walletUpdate.Keys["PubKey"] };
         
                _ = Task.Run(async () => {
                    foreach (var (id, superInfo) in Program.SuperNodes)
                    {
                        
                        if (superInfo.WebSoccket != null)
                        {
                            superInfo.WebSoccket.InvokeAsync("WalletDataUpdate", JsonConvert.SerializeObject(walletUpdate));
                        }
                       
                    }
                });

                return Content(JsonConvert.SerializeObject(walletInDb));
            }
            else
            {

                var walletJoining = new TruWallet();
                walletJoining.PushNotifToken = walletUpdate.PushNotifToken;
                walletJoining.ProfilePicBase64 = walletUpdate.ProfilePicBase64;
                walletJoining.Handle = walletUpdate.Handle;
                walletJoining.FullName = walletUpdate.FullName;
                walletJoining.Keys = new WalletKeys() { PubKey = walletUpdate.Keys["PubKey"] };
      
                Program.NetworkWallets.Add(walletJoining.ID, walletJoining);

                _ = Task.Run(async () => {
                    foreach (var (url, superInfo) in Program.SuperNodes)
                    {
                        if (superInfo.WebSoccket != null)
                        {
                            superInfo.WebSoccket.InvokeAsync("NewWalletJoined", JsonConvert.SerializeObject(walletJoining));
                        }
                    
                    }
                });

                return Content(JsonConvert.SerializeObject(walletJoining));
            }

            Response.StatusCode = 400;
            return Content("Wallet already exists");
        }


        [HttpPost("getWalletByPubKey")]
        public async Task<ActionResult> Post([FromBody] Dictionary<string, string> pubKey)
        {
            var publicKey = pubKey["PubKey"];
            var wallet = Program.NetworkWallets.FindByPubKey(publicKey);


            if (wallet != null)
            {
                return Content(JsonConvert.SerializeObject(wallet));
            }
            else
            {
                //This doesnt work.. like I'd want.. you cant create a new wallet from only a public key...
                var walletJoining = new TruWallet();
                walletJoining.Keys = new WalletKeys() { PubKey = pubKey["PubKey"] };
 
                Program.NetworkWallets.Add(walletJoining.ID, walletJoining);
                return Content(JsonConvert.SerializeObject(walletJoining));
            }
 
        }

  
    }
}
