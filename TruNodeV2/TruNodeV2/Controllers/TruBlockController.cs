﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruNodeV2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruBlockController : Controller
    {
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Content(JsonConvert.SerializeObject(Program.TruLedger.Chain));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(string id)
        {
            var allBalances = Program.TruLedger.CrawlChain();
            if (allBalances.ContainsKey(id))
            {
                return Content(JsonConvert.SerializeObject(allBalances[id]));
            }
            Response.StatusCode = 400;
            return Content("Your Wallet Balance Could Not Be Found");
        }

    }
}
