﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TruNodeV2.Controllers
{

    public class NewTruToken
    {
        public string Name { get; set; }

        public string CreatedBy { get; set; }

        public IFormFile tknFile { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class TruTokenController : Controller
    {
        // GET: api/<ValuesController>
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Content(JsonConvert.SerializeObject(Program.TruTokens)); 
        }

        // GET: api/<ValuesController>
        [HttpGet("walletTokens/{id}")]
        public async Task<ActionResult> Get(string id)
        {

            Dictionary<string, TruToken> myTokens = Program.TruTokens.Where( pair => pair.Value.CreatedBy == id).ToDictionary(i => i.Key, i => i.Value); ;

            //TODO: Actually need to crawl chain to find ownership!!! update chain to handle this:)


            return Content(JsonConvert.SerializeObject(myTokens));
        }


        // GET: api/<ValuesController>
        [HttpGet("download/{id}")]
        public async Task<ActionResult> GetTokn(string id)
        {
            var tkn = Program.TruTokens.Values.FirstOrDefault(tkn => tkn.ID == id);
            if (tkn != null)
            {
                var fileName = tkn.ID + "." + tkn.FileExt;
                byte[] fileBytes = System.IO.File.ReadAllBytes("wwwroot/tokens/" + tkn.ID + "." + tkn.FileExt);
 
                var contentType = "application/octet-stream";
   
                return File(fileBytes, contentType, fileName);
            }
            return NotFound();

        }



        [HttpGet("forsale")]
        public async Task<ActionResult> GetForSale()
        {
            var tokensForSale = Program.TruTokens.Values.Where(tkn => tkn.ForSale);
            return Content(JsonConvert.SerializeObject(tokensForSale));

        }

        [HttpPut("putforsale")]
        public async Task<ActionResult> PutForSale([FromBody] TruToken token)
        { 
            var tkn = Program.TruTokens.Values.FirstOrDefault(tkn => tkn.ID == token.ID);
            if(tkn != null)
            {
                tkn.ForSale = true;
                tkn.SalePrice = token.SalePrice;
                return Content(JsonConvert.SerializeObject(tkn));
            }

            return NotFound();

        }

        [HttpPost("buytoken")]
        public async Task<ActionResult> BuyToken()
        {
            var tokensForSale = Program.TruTokens.Values.Where(tkn => tkn.ForSale);
            return Content(JsonConvert.SerializeObject(tokensForSale));

        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> GetTknById(string id)
        {
            if (Program.TruTokens.ContainsKey(id))
            {
                return Content(JsonConvert.SerializeObject(Program.TruTokens[id]));
            }
            return NotFound();
        }



        // POST api/<ValuesController>
        [HttpPost]
        public async Task<ActionResult> Post([FromForm] NewTruToken tkn)
        {
            var newToken = new TruToken();

            newToken.Name = tkn.Name;
           
            var fileNameSplit = tkn.tknFile.FileName.Split(".");
            newToken.FileExt = fileNameSplit[1];
            newToken.CreatedBy = tkn.CreatedBy;
            string tokenPath = "wwwroot/tokens";

            if (!Directory.Exists(tokenPath))
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(tokenPath);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(tokenPath));
            }
             
            string path = Path.Combine(tokenPath,  newToken.ID + "." + newToken.FileExt);
 
            using (Stream stream = new FileStream(path, FileMode.Create))
            {
                tkn.tknFile.CopyTo(stream);
            }


            Program.TruTokens.Add(newToken.ID, newToken);


            return Content(JsonConvert.SerializeObject(newToken));
        }

       
    }
}
