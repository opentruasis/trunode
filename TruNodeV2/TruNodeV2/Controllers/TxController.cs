﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.Controllers
{
 
    [Route("api/[controller]")]
    [ApiController]
    public class TxController : ControllerBase
    {
 
        [HttpPost]
        public async Task<ActionResult> PostTx([FromBody] TruTxDTO newTxFromClient)
        {
            Console.WriteLine("New TX>>>>");
 
            var newTx = new TruTx(newTxFromClient);
            Console.WriteLine(newTx);

            if (Program.NetworkWallets.ContainsKey(newTx.SignerId))
            {
                //verify msg!!
                //Get Wallet By Signer ID>>>
                var inNetworkWallet = Program.NetworkWallets[newTx.SignerId];

                if (newTx.isValidSig(inNetworkWallet.Keys.PubKey))
                {

                    //Make sure Signer ID == SenderID..
                    newTx.CreatedOn = DateTime.UtcNow.ToString();

                    ///some percent of conneced wallets.. + another smaller percent of somewhere else on the net.
                    // 
                    var localCount = (int)(Program.ConnectedWallets.Count * 0.5);
                    var otherSuperNodes = Program.SuperNodes.Count;
                    var otherSuperNodeFullNodeConnectedWallets = 0;
 
                    foreach (var (key, val) in Program.SuperNodes)
                    {
                        if (val.HeartBeat != null)
                        {
                            otherSuperNodeFullNodeConnectedWallets += val.HeartBeat.connectedFullNodeCount;
                        }
                    }
                
                    newTx.ExpectedValidationResponses = (int)((localCount + otherSuperNodes + otherSuperNodeFullNodeConnectedWallets + 1) * 0.5);
                    Debug.WriteLine(newTx.ExpectedValidationResponses);
                    lock ( Program.ConsensusManager.ValidationPool ) { 
                        var existingSameTx = Program.ConsensusManager.ValidationPool.Find(tx => tx.Signature == newTx.Signature);
                        if (existingSameTx == null)
                        {

                            foreach (var (url, superInfo) in Program.SuperNodes)
                            {
                                if (url != Program.MyURL)
                                {
                                    if (superInfo.WebSoccket != null)
                                    {
                                        NewTxAtOtherSuperDTO newTxSendOverToOtherSupers = new NewTxAtOtherSuperDTO();
                                        newTxSendOverToOtherSupers.NewTx = newTxFromClient;
                                        newTxSendOverToOtherSupers.SourceSuperID = Program.MySuperNode.ID;
                                        superInfo.WebSoccket.InvokeAsync("NewTxAtOtherSuperPosted", JsonConvert.SerializeObject(newTxSendOverToOtherSupers));
                                    }
                                }
                                else
                                {  //This super node adds verification...
                                    TruTxVerify verification = new TruTxVerify();
                                    verification.SignerId = Program.MySuperNode.ID;
                                    verification.CurrentBlockHash = Program.TruLedger.GetCurrentBlockHash();
                                    verification.TxID = newTx.ID;
                                    verification.SenderBalance = Program.TruLedger.GetBalanceAt(newTx.SignerId);
                                    //Add Validation from self..
                                    newTx.PutValidation(verification);
                                }
                            }


                            Program.ConsensusManager.ValidationPool.Add(newTx);
                            /// Program.TruLedger.AddNewTx(newTx);
                            return Content(JsonConvert.SerializeObject(newTx));
                        
                        }
                        else
                        {
                            Response.StatusCode = 400;
                            return Content("Payment Not Accepted to Network - Already Posted");
                        }
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    return Content("Payment Not Accepted to Network - Bad Signature");
                }

            }
            
            Response.StatusCode = 400;
            return Content("Payment Not Accepted to Network - Sender is not in newtork");
             
        }



    }
}
