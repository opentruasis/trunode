﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruNodeV2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruConsensusController : Controller
    {
        [HttpGet]
        public async Task<ActionResult> Get()
        {
 
            return Content(JsonConvert.SerializeObject(Program.ConsensusManager.DTO));
        }



    }
}
