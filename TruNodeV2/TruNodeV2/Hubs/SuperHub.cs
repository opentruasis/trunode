﻿using System;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using TruV2Shared;
using Microsoft.AspNetCore.SignalR.Client;
using TruV2Shared.TruDataUnits;
using TruNodeV2.TruConsensus;
using TruSharedV2;

namespace TruNodeV2.Hubs
{
    public partial class SuperHub: Hub  /////SERVER SIDE
    {

        public async Task Version(string msg)
        {
            var version = JsonConvert.DeserializeObject<TruVersion>(msg);

            if(version.Version != Program.Version)
            {
                Clients.Client(version.connectionId).SendAsync("Disconnecting");
                return;
            }

            var versionAck = new TruVersionAck()
            {
                Version = Program.Version,
                SuperNodeInfo = Program.MySuperNode
            };

            if (!Program.SuperNodes.ContainsKey(version.SuperNodeInfo.ID))
            { 
                Program.SuperNodes.Add(version.SuperNodeInfo.ID, version.SuperNodeInfo);
            }


            if (!Program.NetworkWallets.ContainsKey(version.SuperNodeInfo.ID))
            {
                SendGetWalletRequestById(version.connectionId, version.SuperNodeInfo.ID);
            }

            await Clients.Client(version.connectionId).SendAsync("VersionAck", JsonConvert.SerializeObject(versionAck));
        }


 
        public async Task HeartBeat(string msg)
        {
           // System.Console.WriteLine(heartbeat);
           
            var heartbeatMsg = JsonConvert.DeserializeObject<SuperHeartbeatDTO>(msg);

            if (Program.SuperNodes.ContainsKey(heartbeatMsg.SuperURL))
            {
                Program.SuperNodes[heartbeatMsg.SuperURL].HeartBeat = heartbeatMsg;
                Program.SuperNodes[heartbeatMsg.SuperURL].ID = heartbeatMsg.superNodeInfo.ID;
                Program.SuperNodes[heartbeatMsg.SuperURL].PubKey = heartbeatMsg.superNodeInfo.PubKey;
                if(Program.ConsensusManager.SuperNodeLeader != heartbeatMsg.currentSuperLeader)
                {
                    //   Program.ConsensusManager.SuperNodeLeader = heartbeatData.currentSuperLeader;
                    Console.WriteLine("HearBeat Leaderss: " + Program.ConsensusManager.SuperNodeLeader + " ++++ " + heartbeatMsg.currentSuperLeader);

                    //if (Program.ConsensusManager.ElectionState == LeaderElectionState.DONE)
                    //{  //Send my vote out only once.
                       
                    //    List<string> superIdList = Program.GetSuperNodeIdList();

                    //    var leaderNewLeaderVote = Program.ConsensusManager.LeaderElectionPool.Vote(
                    //            Program.ConsensusManager.ElectionCycle,
                    //            Program.ConsensusManager.SuperNodeLeader,
                    //            superIdList
                    //        );
                    //    Console.WriteLine("Sending new Vote: " + leaderNewLeaderVote.VotedSuperId);
                    //    Program.ConsensusManager.ElectionCycle++;
                    //    Program.ConsensusManager.ElectionState = LeaderElectionState.START;
                    //    Decentralize.BroadcastTruConsensusMsg(leaderNewLeaderVote);
                    //}
                }
            }
          

            if (!Program.SuperUrls.URLS.Contains(heartbeatMsg.SuperURL))
            {
                Program.SuperUrls.Add(heartbeatMsg.SuperURL);
            }

            SendChainDataIfNeeded(heartbeatMsg.connectionId, heartbeatMsg.endblockID);

            SendSuperUrlsIfNeeded(heartbeatMsg.connectionId, heartbeatMsg.SuperUrlListCount);

            SendWalletInfoIfNeeded(heartbeatMsg.connectionId, heartbeatMsg.superNodeInfo.WalletCount);

            var heartBeatResp = new HeartBeatRespDTO() { heartbeat = heartbeatMsg.heartbeat, walletId = Program.MySuperNode.ID };

            await Clients.Client(heartbeatMsg.connectionId).SendAsync("HeartbeatResponse", JsonConvert.SerializeObject(heartBeatResp));
        }


        private async void SendChainDataIfNeeded(string connectionId, long userEndBlockID)
        {
            if (Program.TruLedger.EndBlockId > userEndBlockID)
            {
                var runnout = Program.TruLedger.GetSomeLedger(userEndBlockID, 5);
                await Clients.Client(connectionId).SendAsync("ChainData", JsonConvert.SerializeObject(runnout));
            }
        }


        private async void SendSuperUrlsIfNeeded(string connectionId, int superUrlsCount)
        {
            if (Program.SuperUrls.Count > superUrlsCount)
            { 
                await Clients.Client(connectionId).SendAsync("SuperUrlList", JsonConvert.SerializeObject(Program.SuperUrls.URLS));
            }
        }

 
        private void SendWalletInfoIfNeeded(string connectionId, long walletCount)
        {
            Console.WriteLine("My Wallet COunt: " + Program.NetworkWallets.Count + " = " + walletCount);
            if (Program.NetworkWallets.Count != walletCount)
            {
                Clients.Client(connectionId).SendAsync("NetworkWallets", JsonConvert.SerializeObject(Program.NetworkWallets.Wallets));
            }
        }

        private void SendGetWalletRequestById(string connectionId, string walletId)
        {
             Clients.Client(connectionId).SendAsync("GetNetworkWallet", walletId);
        }

        

        public async Task NewWalletJoined(string newWalletStr)
        {
            System.Console.WriteLine(newWalletStr);

            var newWallet = JsonConvert.DeserializeObject<WalletDataDTO>(newWalletStr);
            var wallet = new TruWallet()
            {
                ID = newWallet.ID,
                Keys = new WalletKeys() { PubKey = newWallet.Keys["PubKey"] },
                PushNotifToken = newWallet.PushNotifToken,
                FullName = newWallet.FullName,
                Handle = newWallet.Handle,
                Balance = newWallet.Balance
            };

            if (Program.NetworkWallets.ContainsKey(wallet.ID)){
               
                Program.NetworkWallets[wallet.ID] = wallet;
            }
            else
            {
                Program.NetworkWallets.Add(wallet.ID, wallet);
            }
        }



        public async Task WalletDataUpdate(string walletDataStr)
        {
            System.Console.WriteLine(walletDataStr);

            var newWallet = JsonConvert.DeserializeObject<WalletDataDTO>(walletDataStr);

            var wallet = new TruWallet()
            {
                ID = newWallet.ID,
                ProfilePicBase64 = newWallet.ProfilePicBase64,
                Keys = new WalletKeys() { PubKey = newWallet.Keys["PubKey"] },
                PushNotifToken = newWallet.PushNotifToken,
                FullName = newWallet.FullName,
                Handle = newWallet.Handle,
                Balance = newWallet.Balance
            };

            if (Program.NetworkWallets.ContainsKey(wallet.ID))
            {
                Program.NetworkWallets[wallet.ID] = wallet;
            }
            else
            {
                Program.NetworkWallets.Add(wallet.ID, wallet);
            }

        }



        public async Task NewTxAtOtherSuperPosted(string newTxData)
        {
            System.Console.WriteLine(newTxData);
            var newSuperNewTxDO = JsonConvert.DeserializeObject<NewTxAtOtherSuperDTO>(newTxData);

            var newTx = new TruTx(newSuperNewTxDO.NewTx);
            Console.WriteLine("---------------------");
            Console.WriteLine(newTx);
            Console.WriteLine(newTx.SignerId);
            Console.WriteLine(Program.NetworkWallets.Count);
            if (Program.NetworkWallets.ContainsKey(newTx.SignerId))
            {
                Console.WriteLine("Signer Is in Network Wallets..");
                //verify msg!!
                //Get Wallet By Signer ID>>>
                var inNetworkWallet = Program.NetworkWallets[newTx.SignerId];

                if (newTx.isValidSig(inNetworkWallet.Keys.PubKey))
                {

                    Console.WriteLine("Is a valid Signature on the TX>>");
                    //Send This super NOde validation back..
                    if (Program.SuperNodes.ContainsKey(newSuperNewTxDO.SourceSuperID))
                    {
                        var OtherSuper = Program.SuperNodes[newSuperNewTxDO.SourceSuperID];
                        TruTxVerify verification = new TruTxVerify();
                        verification.SignerId = Program.MySuperNode.ID;
                        verification.TxID = newTx.ID;
                        verification.SenderBalance = Program.TruLedger.GetBalanceAt(newTx.SignerId);

                        if (OtherSuper.WebSoccket != null)
                        {
                            _ = OtherSuper.WebSoccket.InvokeAsync("GotTxValidation", JsonConvert.SerializeObject(verification));
                        }
                    }


          
                    //Make sure Signer ID == SenderID..
                    newTx.CreatedOn = DateTime.UtcNow.ToString();

                    var existingSameTx = Program.ConsensusManager.ValidationPool.Find(tx => tx.Signature == newTx.Signature);
                    if (existingSameTx == null)
                    {
                        Program.ConsensusManager.ValidationPool.Add(newTx);
                    }
                }
            }
            else
            {
                //Get Wallet//
 
            }

        }

         // check if transactions is valid
//          if (
//            !this.transactionPool.transactionExists(data.transaction) &&
//            this.transactionPool.verifyTransaction(data.transaction) &&
//            this.validators.isValidValidator(data.transaction.from)
//          ) {
//            let thresholdReached = this.transactionPool.addTransaction(
//              data.transaction
//            );
//            // send transactions to other nodes
//            this.broadcastTransaction(data.transaction);

//            // check if limit reached
//            if (thresholdReached) {
//              console.log("THRESHOLD REACHED");
//              // check the current node is the proposer
//              if (this.blockchain.getProposer() == this.wallet.getPublicKey()) {
//            console.log("PROPOSING BLOCK");
//            // if the node is the proposer, create a block and broadcast it
//            let block = this.blockchain.createBlock(
//              this.transactionPool.transactions,
//              this.wallet
//            );
//            console.log("CREATED BLOCK", block);
//            this.broadcastPrePrepare(block);
//        }
//        } else {
//              console.log("Transaction Added");
//            }
//}


        public async Task GotTxValidation(string txValidationJson)
        {
            TruTxVerify verification = JsonConvert.DeserializeObject<TruTxVerify>(txValidationJson);
            try
            {
                lock (Program.ConsensusManager.ValidationPool)
                {
                    var tx = Program.ConsensusManager.ValidationPool.Find(tx => tx.ID == verification.TxID);
                    if (tx != null)
                    {
                        Console.WriteLine("Got TX Validation!!>>>");

                        //Send out verfification to other supers..

                        tx.PutValidation(verification);

                        //Check if tx is valid..

                        if (tx.isVerified)
                        {
                            Program.ConsensusManager.ValidationPool.Remove(tx);

                            if (Program.TruLedger.AddNewTx(tx))
                            {
                                //send updated Last BLOCK>>>
                                Clients.All.SendAsync("ChainData", JsonConvert.SerializeObject(new Dictionary<long, TruBlock> { { Program.TruLedger.EndBlockId, Program.TruLedger.Chain[Program.TruLedger.EndBlockId] } }));
  
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }




        public async Task TruConsensusMsg(string msg)
        {
            try
            {
                var truConsensusMsg = JsonConvert.DeserializeObject<TruConsensusMsg>(msg);

                Console.WriteLine("RECEIVED", truConsensusMsg.MsgType);
 
                switch (truConsensusMsg.MsgType)
                {
                    case ConsensusMsgType.VOTE_RESTART:
                        Console.WriteLine("Got LEADER VOTE");
                        Program.ConsensusManager.LeaderElectionPool.Clear();
                        Program.ConsensusManager.ElectionTimeCounter = DateTime.Now;
                        break;
                    case ConsensusMsgType.LEADER_VOTE:
                        Console.WriteLine("Got LEADER VOTE");
                        var voteMsg = JsonConvert.DeserializeObject<TruSuperVote>(msg);
                        Console.WriteLine(voteMsg.Sequence);
                        Program.ConsensusManager.LeaderElectionPool.AddVote(voteMsg);

                        if (!Program.ConsensusManager.LeaderElectionPool.Pool.ContainsKey(Program.MySuperNode.ID))
                        {  //Send my vote out only once.
                            var now = DateTime.UtcNow;
                            Program.ConsensusManager.ElectionTimeCounter = now;
                            var leaderNewLeaderVote = Program.ConsensusManager.LeaderElectionPool.Vote(
                                Program.ConsensusManager.ElectionCycle,
                                Program.ConsensusManager.SuperNodeLeader
                            );
                            Program.ConsensusManager.ElectionCycle++;
                       
                            Decentralize.BroadcastTruConsensusMsg(leaderNewLeaderVote);
                       }

                        Console.WriteLine("Pool COunt: " + Program.ConsensusManager.LeaderElectionPool.Pool.Count +">=" + Program.ConsensusManager.ELECTION_MIN);
                        if (Program.ConsensusManager.LeaderElectionPool.Pool.Count >=
                                Program.ConsensusManager.ELECTION_MIN)
                        {

                            Console.WriteLine("Did we elect someone? ");
            
                           //Set leader. send leader commit
                            var leaderChosen = Program.ConsensusManager.LeaderElectionPool.GetNewLeader();

                            if(leaderChosen == null)
                            {
                                //Send Restart.. Msg
                                Decentralize.BroadcastTruConsensusMsg(new LeaderElectRestart());
                            }
                            Console.WriteLine(Program.ConsensusManager.SuperNodeLeader);
                            Console.WriteLine(leaderChosen.NewSuperLeaderId);
                            //Program.ConsensusManager.LeaderElectionPool.Clear();
                            //Send LeaderChosen Msg..
                            if (leaderChosen.NewSuperLeaderId == Program.MySuperNode.ID)
                            {
                                Program.ConsensusManager.SuperNodeLeader = leaderChosen.NewSuperLeaderId;
                                leaderChosen.SignerId = Program.MySuperNode.ID;
                                TruRSACrypto.SignMsg(leaderChosen, Program.MyWallet.Keys.PrivKey);
                                Program.ConsensusManager.LeaderElectionPool.Clear();
                                Decentralize.BroadcastTruConsensusMsg(leaderChosen);
                            }
                           // Clients.All.SendAsync("TruConsensusMsg", JsonConvert.SerializeObject(leaderChosen));
                        }


                        break;
                    case ConsensusMsgType.LEADER_CHOSEN:
                        Console.WriteLine("Got LEADER CHOSEN MSG");
 
                        //Try again.. or do something maybe..
                      
                        var leaderChosenMsg = JsonConvert.DeserializeObject<TruLeaderChosen>(msg);
                   
                        //initiate a recount..

                        var newLeader = Program.ConsensusManager.LeaderElectionPool.GetNewLeader();
                        Console.WriteLine(newLeader);
                        Console.WriteLine("Leader CHosen: ME:: " + newLeader.NewSuperLeaderId + " -- Them: " + leaderChosenMsg.NewSuperLeaderId);
                     
                        Program.ConsensusManager.LeaderElectionPool.Clear();
                        Program.ConsensusManager.SuperNodeLeader = leaderChosenMsg.NewSuperLeaderId;
                         
                        break;

                    case ConsensusMsgType.PRE_PREPARE:

                        // check if block is valid
                        var blockMsg = JsonConvert.DeserializeObject<TruBlockProposal>(msg);
                        if (
                          !Program.ConsensusManager.BlockPool.ExisitingBlock(blockMsg.Block) &&
                           Program.TruLedger.IsValidBlock(blockMsg.Block)
                        )
                        {
                            // add block to pool
                            Program.ConsensusManager.BlockPool.AddBlock(blockMsg.Block);
                            // send to other nodes
                            Clients.All.SendAsync("TruConsensusMsg", JsonConvert.SerializeObject(blockMsg));


                            // create and broadcast a prepare message
                            var prepare = Program.ConsensusManager.PreparePool.Prepare(Program.TruLedger.EndBlock);
                            Clients.All.SendAsync("TruConsensusMsg", JsonConvert.SerializeObject(prepare));

                        }
                        break;
                    case ConsensusMsgType.PREPARE:

                        // check if block is valid
                        var blockPrepare = JsonConvert.DeserializeObject<TruBlockPrepare>(msg);

                        if (
                          !Program.ConsensusManager.PreparePool.ExistingPrepare(blockPrepare) &&
                          Program.TruLedger.IsValidBlock(blockPrepare.Block)
                        )
                        {
                            // add block to pool
                            Program.ConsensusManager.BlockPool.AddBlock(blockPrepare.Block);
                            // send to other nodes
                            Clients.All.SendAsync("TruConsensusMsg", JsonConvert.SerializeObject(blockPrepare));


                            // create and broadcast a prepare message
                            var prepare = Program.ConsensusManager.PreparePool.Prepare(Program.TruLedger.EndBlock);
                            Clients.All.SendAsync("TruConsensusMsg", JsonConvert.SerializeObject(prepare));

                        }

                        break;
                    case ConsensusMsgType.COMMIT:

                        var commitMsg = JsonConvert.DeserializeObject<TruBlockCommit>(msg);
                        // check if block is valid
                        // check the validity commit messages
                        if (
                          !Program.ConsensusManager.CommitPool.ExistingCommit(commitMsg) &&
                         Program.ConsensusManager.CommitPool.IsValidCommit(commitMsg) &&
                          Program.TruLedger.IsValidBlock(commitMsg.Block)
                        )
                        {
                            // add to pool
                            Program.ConsensusManager.CommitPool.AddCommit(commitMsg);

                            // send to other nodes
                            Decentralize.BroadcastTruConsensusMsg(commitMsg);  //This node receives block from everyone else.

                            // if no of commit messages reaches minimum required
                            // add updated block to chain
                            if (
                               Program.ConsensusManager.CommitPool.Pool[commitMsg.Block.BlockHash].Count >=
                               Program.ConsensusManager.MIN_AGREEMENTS
                            )
                            {
                                Program.TruLedger.Chain[Program.TruLedger.EndBlockId] = commitMsg.Block;
                                Program.TruLedger.AppendChain();
                                Decentralize.ResetConsensusManager();

                            }
                            else
                            {
                                // Send a round change message to nodes
                                var message = Program.ConsensusManager.RoundChangePool.ChangeRound(Program.TruLedger.EndBlock);
                                Decentralize.BroadcastTruConsensusMsg(message);
                            }
                        }


                        break;
                    case ConsensusMsgType.ROUND_CHANGE:

                        var roundChangeMsg = JsonConvert.DeserializeObject<RoundChangeMsg>(msg);
                        if (
                           !Program.ConsensusManager.RoundChangePool.ExistingMessage(roundChangeMsg) &&
                           Program.ConsensusManager.RoundChangePool.IsValidMessage(roundChangeMsg)
                         )
                        {
                            // add to pool
                            Program.ConsensusManager.RoundChangePool.AddMessage(roundChangeMsg);

                            // send to other nodes
                            Decentralize.BroadcastTruConsensusMsg(roundChangeMsg);  //This node receives block from everyone else.

                            // if enough messages are received, clear the pools
                            if (
                               Program.ConsensusManager.RoundChangePool.Pool[roundChangeMsg.Block.BlockHash].Count >=
                                Program.ConsensusManager.MIN_AGREEMENTS
                            )
                            {

                                // Set the heaviest last block.
                                if (
                                    Program.ConsensusManager.CommitPool.Pool[roundChangeMsg.Block.BlockHash].Count >=
                                    Program.ConsensusManager.MIN_AGREEMENTS
                                )
                                {
                                    Program.TruLedger.Chain[Program.TruLedger.EndBlockId] = roundChangeMsg.Block;
                                    Program.TruLedger.AppendChain();

                                }
                                else
                                {
                                    Program.TruLedger.Chain[Program.TruLedger.EndBlockId] = Program.ConsensusManager.BlockPool.GetHeaviestValidBlock();
                                }
                                // Find new leader.


                            }
                        }
                        break;
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
