﻿using System;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TruV2Shared;
using System.Linq;
using Microsoft.AspNetCore.SignalR.Client;
using TruV2Shared.TruDataUnits;

namespace TruNodeV2.Hubs
{

    public class TruClientHub : Hub
    {
    
        public async Task ClientDisconnect(string connectionId)
        {
            try
            {
                Console.WriteLine("Client Disconnect");
                Console.WriteLine(connectionId);
                if (Program.ConnectedWallets.ContainsKey(connectionId))
                {
                    Program.ConnectedWallets.Remove(connectionId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task Heartbeat(string heartbeat)
        {
            try
            {
                var heartbeatData = JsonConvert.DeserializeObject<HeartbeatDTO>(heartbeat);
                string userId = heartbeatData.connectionId;
                long userEndBlock =  heartbeatData.endblockID;

                heartbeatData.hbtime = DateTime.UtcNow;

                if (Program.ConnectedWallets.ContainsKey(userId))
                {
                    Program.ConnectedWallets[userId] = heartbeatData;
                }
                else
                {
                    Program.ConnectedWallets.Add(userId, heartbeatData);
                }
 
                SendSuperNodeListIfNeeded(userId, heartbeatData.superNodeCount);

                SendChainDataIfNeeded(userId, heartbeatData.endblockID);

                await Clients.Client(userId).SendAsync("HeartbeatResponse", heartbeat);
                //await Clients.All.SendAsync("HeartbeatResponse", heartbeat);
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private async void SendChainDataIfNeeded(string userId, long userEndBlockID)
        {
            if (Program.TruLedger.EndBlockId > userEndBlockID) {
                var runnout = Program.TruLedger.GetSomeLedger(userEndBlockID, 5);
                await Clients.Client(userId).SendAsync("ChainData", JsonConvert.SerializeObject(runnout));
            }
        }
 

        public async Task LoadChainBlocks(string chainDataRequestJson)
        {
            try
            {
                var dataRequest = JsonConvert.DeserializeObject<ChainDataRequestDTO>(chainDataRequestJson);
                if (Program.TruLedger.EndBlockId > dataRequest.blockNum)
                {
                    var runnout = Program.TruLedger.GetSomeLedger(dataRequest.blockNum, 5);
                    await Clients.Client(dataRequest.HubConnectionId).SendAsync("ChainData", JsonConvert.SerializeObject(runnout));
                }
            }catch(Exception e)
            {

            }
        }

        private async void SendSuperNodeListIfNeeded(string userId, int superNodeCount)
        {
            if (Program.SuperNodes.Count != superNodeCount)
            {
                var listOfSuperURLS = new List<string>();
                var superNodeList = Program.SuperNodes.Values.ToList();
                foreach (var super in superNodeList)
                {
                    listOfSuperURLS.Add(super.SuperURL);
                }

                Clients.Client(userId).SendAsync("SuperNodeList", JsonConvert.SerializeObject(listOfSuperURLS));
            }
        }
 

        public async Task TxValidation(string txValidationJson)
        {
            TruTxVerify verification = JsonConvert.DeserializeObject<TruTxVerify>(txValidationJson);
            try
            {
                lock (Program.ConsensusManager.ValidationPool)
                {
                    var tx = Program.ConsensusManager.ValidationPool.Find(tx => tx.ID == verification.TxID);
                    if (tx != null)
                    {

                        //Send out verfification to other supers..
                        _ = Task.Run(async () =>
                        {
                            foreach (var (id, superInfo) in Program.SuperNodes)
                            {
                                if (id != Program.MySuperNode.ID && superInfo.SuperURL != Program.MyURL)
                                {
                                    if (superInfo.WebSoccket != null)
                                    {
                                        superInfo.WebSoccket.InvokeAsync("GotTxValidation", txValidationJson);
                                    }
                                }
                            }
                        });

                        if (verification.CurrentBlockHash == Program.TruLedger.GetCurrentBlockHash())
                        {
                            tx.PutValidation(verification);

                            //Check if tx is valid..
                            if (tx.isVerified)
                            {
                                Program.ConsensusManager.ValidationPool.Remove(tx);

                                if (Program.TruLedger.AddNewTx(tx))
                                {
                                    //send updated Last BLOCK>>>
                                    Clients.All.SendAsync("ChainData", JsonConvert.SerializeObject(new Dictionary<long, TruBlock> { { Program.TruLedger.EndBlockId, Program.TruLedger.Chain[Program.TruLedger.EndBlockId] } }));
                                }
                            }
                        }

                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
 
    }
}
