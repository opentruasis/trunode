﻿using System;

namespace TruV2Shared
{
    public class HeartbeatDTO
    {
        public string walletId { get; set; }
        public int heartbeat { get; set; }
        public long endblockID { get; set; }
        public string connectionId { get; set; }
        public int superNodeCount { get; set; }

        public DateTime hbtime { get; set; }
    }
}
