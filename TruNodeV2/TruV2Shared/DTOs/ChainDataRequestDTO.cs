﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruV2Shared
{
    public class ChainDataRequestDTO
    {
        public string HubConnectionId { get; set; }

        public long blockNum { get; set; }
    }
}
