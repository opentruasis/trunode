﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.DTOs
{
    public class SuperURLListDTO : TruDTO
    {

        public TruMsgType Type { get => TruMsgType.SuperURLList; set { } }
        public string SenderId { get; set; }
        public string ConnectionID { get; set; }
 

        public List<string> UrlList { get; set; }
    }
}
