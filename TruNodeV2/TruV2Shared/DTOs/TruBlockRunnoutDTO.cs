﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.TruDataUnits;

namespace TruV2Shared.DTOs
{
    public class TruBlockRunnoutDTO : TruDTO
    {
        public TruMsgType Type { get => TruMsgType.ChainData; set { } }
        public string SenderId { get; set; }
        public string ConnectionID { get; set; }
 
        public SortedDictionary<long, TruBlock> Runnout { get; set; }
    }
}
