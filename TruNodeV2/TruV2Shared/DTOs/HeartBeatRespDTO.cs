﻿using TruV2Shared.DTOs;

namespace TruV2Shared
{
    public class HeartBeatRespDTO : TruDTO
    {
        public TruMsgType Type { get => TruMsgType.SuperHeartBeatResp; set { } }
        public string SenderId { get; set; }
        public string ConnectionID { get; set; }
 
        public string walletId { get; set; }
        public int heartbeat { get; set; }
 
    }
}