﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruV2Shared
{
    public class NewTxAtOtherSuperDTO
    { 
        public string SourceSuperID { get; set; }
        public TruTxDTO NewTx { get; set; }
    }
}
