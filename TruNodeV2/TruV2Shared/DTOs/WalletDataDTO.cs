﻿using System.Collections.Generic;

namespace TruV2Shared
{
    public class WalletDataDTO
    {
        public string ID { get; set; }
        public Dictionary<string,string> Keys { get; set; }
        public string Handle { get; set; }
        public string FullName { get; set; }
        public string PushNotifToken { get; set; }
        public string ProfilePicBase64 { get; set; }
        public double Balance { get; set; } = 0;
    }
}
