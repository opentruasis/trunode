﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.DTOs
{
    public interface TruDTO
    {
        public TruMsgType Type { get; set; }

        public string SenderId { get; set; }

        public string ConnectionID { get; set; }
    }
}
