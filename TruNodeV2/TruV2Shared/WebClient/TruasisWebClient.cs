﻿using Newtonsoft.Json;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TruShared;

namespace TruShared
{
    public class TruasisWebClient
    {
        private string _authToken { get; set; }

        private HttpClient client = new HttpClient();
        private string _baseURL { get; set; } = "https://api.truasis.com";

        private string _secret { get; set; } = "AuthKeySecret";
        private string _key { get; set; } = "AuthKey";

        public bool Connected = false;

        public TruasisWebClient() { }
        public TruasisWebClient(string endpoint) {
            _baseURL = endpoint;
        }
        public async Task<bool> Initialize()
        {
            // Get JWT Token to initialize
            var auth = GetAuthCreds().Result;

            _key = auth.Item1;
            _secret = auth.Item2;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
 
            return await GetToken();
        }

        public async Task<dynamic> DownloadJobParamFile(string v, FileEncoding type = FileEncoding.StringType)
        {
            string endpoint = @"/jobParamFile/" + v;
           
            var response = await client.GetAsync(_baseURL + endpoint);
      
            if (response.IsSuccessStatusCode)
            {
                if (type == FileEncoding.StringType)
                {
                    return await response.Content.ReadAsStringAsync();
                }

                if(type == FileEncoding.BinaryType)
                {
                    return await response.Content.ReadAsByteArrayAsync();
                }
            }
            return null;
        }

        private async Task<Tuple<string,string>> GetAuthCreds()
        // My Idea is to get Password Creds from api.truasis.com
        // Call "Some" endpoint... at API.TRUASIS.com.. returns creds.. 
        // I thinks... I can validate call by checking the ID..
        // If.. Super NOde POST includes ID and has already registered.. /// 
        // If TrustScore is okay.. then pass Auth Creds.. NICE I like this.. 
        //TODO> Once this is killing it:) 
        {
            return new Tuple<string, string>("tjgersho", "123123");
        }


        private async Task<bool> GetToken()
        {
            string endpoint = @"/user/login";
            var json = JsonConvert.SerializeObject(new Dictionary<string, dynamic>()
            {
                {"emailorusername", _key },
                {"password", _secret }
            });

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            //FormUrlEncodedContent form = new FormUrlEncodedContent(new[]{
            //        new KeyValuePair<string, string>("emailorusername", _key),
            //        new KeyValuePair<string, string>("password", _secret)
            //    });

            var response = await client.PostAsync(_baseURL + endpoint, content);
            Console.WriteLine("ARE WE GETTING the TokEN?");

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("GOT API.TRUASIS.COM -- TOKEN");
                var resp = await response.Content.ReadAsStringAsync();
                var deserialized = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(resp);
                dynamic tkn;
                var rc = deserialized.TryGetValue("token", out tkn);
                _authToken = (string)tkn;
                Console.WriteLine(_authToken);
                client.DefaultRequestHeaders.TryAddWithoutValidation("APIAuthToken", _authToken);
                Connected = true;
            }
            else
            {
                Connected = false;
            }
            return Connected;
        }


        public async Task<List<RunningAd>> GetRunningAds()
        {
            string endpoint = @"/ad";
            var response = await client.GetAsync(_baseURL + endpoint);
            List<RunningAd> adList = new List<RunningAd>();
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
 
                dynamic deserialized = JsonConvert.DeserializeObject(res);
                try
                {
                    foreach (dynamic eg in deserialized)
                    {
                        ///  RunningAd srl = new RunningAd();
                        RunningAd srl = eg.ToObject<RunningAd>();
                        adList.Add(srl);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
                return adList;
            }
            return null;
        }



        public async Task<bool> UpdateAd(string adId, Dictionary<string, dynamic> data)
        {
            string endpoint = @"/ad/" + adId;
            Console.WriteLine();
            Console.WriteLine("UPdate AD Endpoint: " + _baseURL + endpoint);
            Console.WriteLine();
            var json = JsonConvert.SerializeObject(data);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PutAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                Console.WriteLine(resp);
                
            }
            else
            {
                Console.WriteLine("Update Response failed");
                Console.WriteLine(response.IsSuccessStatusCode);
            }
            return response.IsSuccessStatusCode;
        }


        public async Task<List<JobTypeSchema>> GetJobTypes()
        {
            string endpoint = @"/jobtype";
            var response = await client.GetAsync(_baseURL + endpoint);
            List<JobTypeSchema> jobTypeList = new List<JobTypeSchema>();
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();

                dynamic deserialized = JsonConvert.DeserializeObject(res);
                try
                {
                    foreach (dynamic eg in deserialized)
                    {
                        ///  RunningAd srl = new RunningAd();
                        JobTypeSchema srl = eg.ToObject<JobTypeSchema>();
                        jobTypeList.Add(srl);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }

                return jobTypeList;
            }
            return null;
        }



        public async Task<List<ActiveJob>> GetAvailableJobs()
        {
            string endpoint = @"/job";

            var response = await client.GetAsync(_baseURL + endpoint);
            List<ActiveJob> jobList = new List<ActiveJob>();
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                dynamic deserialized = JsonConvert.DeserializeObject(res);
                try
                {
                    foreach (dynamic eg in deserialized)
                    {
                        Console.WriteLine(eg);
                        ///  RunningAd srl = new RunningAd();
                        ActiveJob srl = eg.ToObject<ActiveJob>();
                        jobList.Add(srl);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }

                return jobList;
            }
            return null;
        }


        public async Task<bool> UpdateJob(string jobId, Dictionary<string, dynamic> data)
        {
            string endpoint = @"/job/" + jobId;
            var json = JsonConvert.SerializeObject(data);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PutAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                Console.WriteLine(resp);

            }
            else
            {
                Console.WriteLine("Update Response failed");
                Console.WriteLine(response.IsSuccessStatusCode);
            }
            return response.IsSuccessStatusCode;
        }

        public async Task<Dictionary<string,string>> UploadResultFile(string filePath, string jobId)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"File [{filePath}] not found.");
            }
            string endpoint = @"/upload/jobResultFile/";
            using (var form = new MultipartFormDataContent()) {
                using (var fileContent = new ByteArrayContent(File.ReadAllBytes(filePath)))
                {
                    fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                    form.Add(fileContent, "file", Path.GetFileName(filePath));
                    form.Add(new StringContent(jobId), "jobId");
 
                    var response = await client.PostAsync(_baseURL + endpoint, form);
                
                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();

                        Dictionary<string, string> deserialized = JsonConvert.DeserializeObject<Dictionary<string,string>>(responseContent);

                        return deserialized;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
     
        //public async Task<bool> AddResult(string jobId, Dictionary<string, dynamic> data)
        //{
        //    string endpoint = @"/jobResults/job/" + jobId;
        //    var json = JsonConvert.SerializeObject(data);

        //    var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
 
        //    var response = await client.PostAsync(_baseURL + endpoint, content);

        //    if (response.IsSuccessStatusCode)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
            
        //}


        public async Task<bool> AddResult(string jobId, dynamic result)
        {
            string endpoint = @"/jobResults/job/" + jobId;
            var json = JsonConvert.SerializeObject(result);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public async Task<bool> FinalizeTruPaymentForTru(string txID)
        //{
        //    string endpoint = @"/payments/finalizeTruPayment/" + txID;
      
        //    var response = await client.PutAsync(_baseURL + endpoint, null);

        //    if (response.IsSuccessStatusCode)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
 
        //}

        //public async Task<dynamic> GetStatusPaymentForTru(string txID)
        //{
        //    Console.WriteLine("GET TRU PAYMENT STATUS>>>");
        //    Console.WriteLine(txID);
        //    string endpoint = @"/payments/getTruPaymentStatus/" + txID;

        //    var response = await client.GetAsync(_baseURL + endpoint);

        //    if (response.IsSuccessStatusCode)
        //    {
        //        Console.WriteLine("Response ON getTruPaymentStatus");
        //        Console.WriteLine(response);

        //        //ex resp { "_id":"601d833716266522e90bb313","portal":"paypal","type":"truPurchase","identifyer":"7MU4986559643783P","total":1.5,"paypalEmail":"truasis-buyer@truasis.com","distributed":true,"__v":0}

        //        var res = await response.Content.ReadAsStringAsync();
        //        dynamic deserialized = JsonConvert.DeserializeObject(res);
        //        Console.WriteLine(deserialized);
        //        Console.WriteLine("Was it distributed?");
        //        Console.WriteLine((bool)deserialized.distributed);

        //        var total = (double)deserialized.total;
        //        Console.WriteLine("Total??");
        //        Console.WriteLine(total);
        //        var exchange = (double)deserialized.exchangeRate;

        //        Console.WriteLine("exchange??");
        //        Console.WriteLine(exchange);

        //        if ((bool)deserialized.distributed)
        //        {

        //            return new Dictionary<string, dynamic> { { "distributed", true }, { "total", total }, { "exchangeRate", exchange } };
        //        }
        //        else
        //        {
        //            return new Dictionary<string, dynamic> { { "distributed", false }, { "total", total }, { "exchangeRate", exchange } };
        //        }
        //    }
        //    else
        //    {
        //        return new Dictionary<string, dynamic> { { "distributed", false } };
        //    }

        //}


        public async Task<bool> SaveTransaction(dynamic tx)
        {

            string endpoint = @"/transactions/";
            var json = JsonConvert.SerializeObject(tx);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<bool> NewWalletJoined(dynamic tx)
        {

            string endpoint = @"/wallets/";
            var json = JsonConvert.SerializeObject(tx);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<bool> NewSuperNodeJoined(dynamic tx)
        {

            string endpoint = @"/superNodes/";
            var json = JsonConvert.SerializeObject(tx);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> CheckPaymentTxID(string transactionID) // Returns True if the TX exists else false;
        {
            string endpoint = @"/transactions/findbytx/" + transactionID;
     
            var response = await client.GetAsync(_baseURL + endpoint);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}
