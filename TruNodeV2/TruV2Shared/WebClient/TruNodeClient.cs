﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared;
using TruV2Shared.TruDataUnits;

namespace TruShared.WebClient
{
    public class TruNodeClient
    {
        private HttpClient client = new HttpClient();

#if (DEBUG)
        private string _baseURL { get; set; } = "https://localhost:44380";
#elif(RELEASE)
        private string _baseURL { get; set; } = "http://18.144.168.127:5001";
#endif
 
        public async Task<TruTx> PostTx(TruTxDTO newTx)
        { 
            string endpoint = @"/api/Tx";
     
            var json = JsonConvert.SerializeObject(newTx);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
  
            var response = await client.PostAsync(_baseURL + endpoint, content);
 
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                TruTx deserialized = null;
                try
                {
                    deserialized = JsonConvert.DeserializeObject<TruTx>(res);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
                return deserialized;
            }
            return null;
        }



    }
}
