﻿using Newtonsoft.Json;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TruExchange.Enums;
using TruExchange.Models;
using TruShared;

namespace TruShared
{
    public class TruasisExchangeClient
    {
        private HttpClient client = new HttpClient();
        
       // private string _baseURL { get; set; } = "http://localhost:5000";
        private string _baseURL { get; set; } = "https://ex.truasis.com";

        public bool Connected = false;

        private bool isInitialized = false;

        public void Initialize()
        {
            if (!isInitialized)
            {
                isInitialized = true;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            }
        }

        public async Task<TruPrice> GetLastPrice()
        {
            string endpoint = @"/api/truprice/last";
            var response = await client.GetAsync(_baseURL + endpoint);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruPrice price = JsonConvert.DeserializeObject<TruPrice>(res);
                    return price;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return new TruPrice();
                }
            }
            return new TruPrice();
        }

      

        public async Task<List<TruPrice>> GetTruPrices()
        {
            string endpoint = @"/api/truprice";
            var response = await client.GetAsync(_baseURL + endpoint);
        
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    List<TruPrice> price = JsonConvert.DeserializeObject<List<TruPrice>>(res);
                    price.Sort((a, b) => {
                        return a.TimeStamp > b.TimeStamp ? 1 : -1;
                    });

                    return price;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }

        public async Task<TruOrder> GetOrderStatus(int exchangeOrderID, OrderType orderType)
        {
            if(orderType == OrderType.Buy)
            {
                return await GetBuyOrder(exchangeOrderID);
            }

            return await GetSellOrder(exchangeOrderID);
        }

        public async Task<TruBuyOrder> GetBuyOrder(int orderId)
        {
            string endpoint = @"/api/trubuy/" + orderId;
            var response = await client.GetAsync(_baseURL + endpoint);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruBuyOrder buyOrder = JsonConvert.DeserializeObject<TruBuyOrder>(res);
                    return buyOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }

        public async Task<TruSellOrder> GetSellOrder(int orderId)
        {
            string endpoint = @"/api/trusell/" + orderId;
            var response = await client.GetAsync(_baseURL + endpoint);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruSellOrder sellOrder = JsonConvert.DeserializeObject<TruSellOrder>(res);
                    return sellOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }

        public async Task<TruBuyOrder> GetBestBid()
        {
            string endpoint = @"/api/truorders/bestbid";
            var response = await client.GetAsync(_baseURL + endpoint);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruBuyOrder buyOrder = JsonConvert.DeserializeObject<TruBuyOrder>(res);
                    return buyOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }



        public async Task<TruSellOrder> GetBestAsk()
        {
            string endpoint = @"/api/truorders/bestask";
            var response = await client.GetAsync(_baseURL + endpoint);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruSellOrder sellOrder = JsonConvert.DeserializeObject<TruSellOrder>(res);
                    return sellOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }




        public async Task<TruSellOrder> PostSellOrder(dynamic tx)
        {
            string endpoint = @"/api/trusell";
 
            var json = JsonConvert.SerializeObject(tx);

            var jsonToString = json.ToString();
            Console.WriteLine(jsonToString);
            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruSellOrder sellOrder = JsonConvert.DeserializeObject<TruSellOrder>(res);
                    return sellOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
       
        }



        public async Task<TruBuyOrder> CancelBuyOrder(int orderID, string walletId)
        {
            string endpoint = @"/api/trubuy/" + orderID;

            var json = JsonConvert.SerializeObject(new Dictionary<string, dynamic> {
                { "WalletId", walletId },
                {"OrderStatus" ,  TruExchange.Enums.OrderStatus.Canceled}
            });

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PutAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruBuyOrder buyOrder = JsonConvert.DeserializeObject<TruBuyOrder>(res);
                    return buyOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;

        }

        public async Task<Dictionary<string, string>> GetMasterWalletInfo()
        {
            string endpoint = @"/api/masterwallet";
            var response = await client.GetAsync(_baseURL + endpoint);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    Dictionary<string, string> walletInfo = JsonConvert.DeserializeObject<Dictionary<string, string>>(res);
                    return walletInfo;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;
        }


        public async Task<TruBuyOrder> PostBuyOrder(TruBuyOrder tx)
        {
            //var tx = new Dictionary<string, dynamic> {  //Useage!
            //        {"Qty" , 1000}, 
            //        {"LimitPrice": 1.1}
            //};

            string endpoint = @"/api/trubuy";

            var json = JsonConvert.SerializeObject(tx);

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruBuyOrder buyOrder = JsonConvert.DeserializeObject<TruBuyOrder>(res);
                    return buyOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;

        }

        public async Task<TruSellOrder> CancelSellOrder(int orderID, string walletId)
        {
            string endpoint = @"/api/trusell/" + orderID;

            var json = JsonConvert.SerializeObject(new Dictionary<string, dynamic> { 
                { "WalletId", walletId }, 
                {"OrderStatus" ,  TruExchange.Enums.OrderStatus.Canceled}
            });

            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            var response = await client.PutAsync(_baseURL + endpoint, content);

            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                try
                {
                    TruSellOrder sellOrder = JsonConvert.DeserializeObject<TruSellOrder>(res);
                    return sellOrder;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Deserialization Exception");
                    return null;
                }
            }
            return null;

        }

    }

}
