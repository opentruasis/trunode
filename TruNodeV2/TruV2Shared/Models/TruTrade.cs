﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruExchange.Enums;

namespace TruExchange.Models
{
    public class TruTrade : IDataModel
    {
        public int ID { get; set; }
        public DateTime TimeStamp { get; set; }
        public int OrderId { get; set; }
        public string MatchIds { get; set; }
        public double Price { get; set; }
        public double Qty { get; set; }
        public OrderType OrderType { get; set; }
    }
}
