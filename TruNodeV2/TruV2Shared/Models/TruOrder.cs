﻿using System;
using TruExchange.Enums;
using TruV2Shared.Interfaces;

namespace TruExchange.Models
{
    public abstract class TruOrder : IDataModel
    {
        public int ID { get; set; }
        public virtual OrderType OrderType {get;}
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public double Qty { get; set; }
        public double CumQty { get; set; }
        public double LeavesQty { get; set; }
        public double LimitPrice { get; set; }
        public OrderStatus OrderStatus { get; set; }
   
    }
}
