﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Models
{
    public class TruPrice : IDataModel
    {
        public int ID { get; set; }
        public DateTime TimeStamp { get; set; }
        public double Price { get; set; }
    }
}
