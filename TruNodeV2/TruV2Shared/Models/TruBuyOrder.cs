﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruExchange.Enums;

namespace TruExchange.Models
{
    public class TruBuyOrder : TruOrder, IDataModel
    {
        public override OrderType OrderType => Enums.OrderType.Buy;
        public string BuyerTruID { get; set; }
        public string BuyerPubKey { get; set; }
        public string PaypalBuyURL { get; set; }
        public string PaypalTx { get; set; }
        public string PaypalBuyerEmail { get; set; }
    }
}
