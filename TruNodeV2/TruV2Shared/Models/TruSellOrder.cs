﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TruExchange.Enums;
using TruV2Shared.Interfaces;

namespace TruExchange.Models
{
    public class TruSellOrder : TruOrder, IDataModel
    {
        public override OrderType OrderType => Enums.OrderType.Sell;
        public string SellerPayPalEmail { get; set; }
        public string PaypalTx { get; set; }
        public string SendTruMsg { get; set; }  // Send Tru Msg.  Signed by the seller to MasterWallet..
    }
}
