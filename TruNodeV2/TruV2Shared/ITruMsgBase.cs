﻿namespace TruV2Shared
{
    public interface ITruMsgBase
    {
        int NetHeader { get; }
        
        TruMsgType MsgType { get; }

        string MsgInfo { get; }

        int Size { get; set; }

        bool Serialize(ref byte[] buffer, ref int offset);

        bool Deserialize(ref byte[] buffer, ref int offset);

        byte[] EncodeSelf();
    }

}