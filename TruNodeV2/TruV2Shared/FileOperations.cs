﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared
{
    public class FileOperations 
    {
 
        public static string ReadToString(string fileName, string path = @"./")
        {
            // deserialize JSON directly from a file

            string _path = Path.Combine(path, fileName);
 
            try
            {
                StreamReader file = File.OpenText(_path);
                string fileContents = file.ReadToEnd();
                file.Close();
                return fileContents;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return "";
        }

        public static bool WriteStringToFile(string data, string fileName = "data.json", string path = @"./")
        {

            string _path = Path.Combine(path, fileName); 

            try
            {
                File.WriteAllText(_path, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }


    }
}
