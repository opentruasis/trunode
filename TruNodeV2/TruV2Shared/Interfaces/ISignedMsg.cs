﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.Interfaces
{
    public interface ISignedMsg
    {
        string SignerId { get; set; }

        string Hash { get; set; }

        string Signature { get; set; }

        string stringifyMsg();

        bool isValidSig(string injectedPubKeyBase64Enc);
    }
}
