﻿using System;
using System.Collections.Generic;
using System.Text;
using TruShared;

namespace TruV2Shared.Interfaces
{
    public interface IDataModel
    {
        int ID { get; set; }
    }
}
