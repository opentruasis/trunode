﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TruV2Shared.Interfaces;
using TruV2Shared.TruDataUnits;

namespace TruV2Shared
{
 
    public static class TruRSACrypto  
    {
        const int RSA_KEY_SIZE = 1024; // DO NOT CHANGE>> unless fix encoding and decoding.
        public static WalletKeys GenerateNewKeys() 
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(RSA_KEY_SIZE);
            // Get key into parameters  
            var encodedPubKey = EncodePublicKey(RSA.ExportParameters(false));
            var encodedPrivKey = EncodedPrivKey(RSA.ExportParameters(true));

            return new WalletKeys() { PubKey = encodedPubKey, PrivKey = encodedPrivKey };
        }

 
  
 
        public static string EncodedPrivKey(RSAParameters privKey)
        {
            return Convert.ToBase64String(SerializePrivKey(privKey));
        }


        public static string EncodePublicKey(RSAParameters pubKey)
        {
            return Convert.ToBase64String(SerializeWalletAddress(pubKey));
        }
       
 
        private static RSAParameters DeserializePubKey(byte[] pubKeyBytes)
        {
            var rsaParam = new RSAParameters();
            var mod = new byte[128];
            var exp = new byte[3];

            var copyFromBytes = new byte[128 + 3];
            Buffer.BlockCopy(pubKeyBytes, 0, copyFromBytes, 0, pubKeyBytes.Length);


            var offset = 0;

            Buffer.BlockCopy(copyFromBytes, offset, mod, 0, mod.Length);
            offset += mod.Length;
            Buffer.BlockCopy(copyFromBytes, offset, exp, 0, exp.Length);
    

            rsaParam.Modulus = mod;
            rsaParam.Exponent = exp;

            return rsaParam;
        }


        private static byte[] SerializeWalletAddress(RSAParameters PubKey)
        {
            var mod = PubKey.Modulus;
            var exp = PubKey.Exponent;
  
            var arr = new byte[
                mod.Length // 128
                + exp.Length // 3
                ];

            int offset = 0;

            Buffer.BlockCopy(mod, 0, arr, offset, mod.Length);
            offset += mod.Length;
            Buffer.BlockCopy(exp, 0, arr, offset, exp.Length);
 
            return arr;
        }


        public static RSAParameters DeserializePrivKey(byte[] privKeyBytes)
        {
            var rsaParam = new RSAParameters();

            var mod = new byte[128];
            var exp = new byte[3];
            var d = new byte[128];
            var p = new byte[64];
            var q = new byte[64];
            var dp = new byte[64];
            var dq = new byte[64];
            var InvQ = new byte[64];

            var copyFromBytes = new byte[128 + 3 + 128 + 64 + 64 + 64 + 64 + 64];
            Buffer.BlockCopy(privKeyBytes, 0, copyFromBytes, 0, privKeyBytes.Length);

            var offset = 0;

            Buffer.BlockCopy(copyFromBytes, offset, mod, 0, mod.Length);
            offset += mod.Length;
            Buffer.BlockCopy(copyFromBytes, offset, exp, 0, exp.Length);
            offset += exp.Length;
            Buffer.BlockCopy(copyFromBytes, offset, d, 0, d.Length);
            offset += d.Length;
            Buffer.BlockCopy(copyFromBytes, offset, p, 0, p.Length);
            offset += p.Length;
            Buffer.BlockCopy(copyFromBytes, offset, q, 0, q.Length);
            offset += q.Length;
            Buffer.BlockCopy(copyFromBytes, offset, dp, 0, dp.Length);
            offset += dp.Length;
            Buffer.BlockCopy(copyFromBytes, offset, dq, 0, dq.Length);
            offset += dq.Length;
            Buffer.BlockCopy(copyFromBytes, offset, InvQ, 0, InvQ.Length);


            rsaParam.Modulus = mod;
            rsaParam.Exponent = exp;
            rsaParam.D = d;
            rsaParam.P = p;
            rsaParam.Q = q;
            rsaParam.DP = dp;
            rsaParam.DQ = dq;
            rsaParam.InverseQ = InvQ;

            return rsaParam;
        }

        private static byte[] SerializePrivKey(RSAParameters PrivKey)
        {
            var mod = PrivKey.Modulus;
            var exp = PrivKey.Exponent;
            var d = PrivKey.D;
            var p = PrivKey.P;
            var q = PrivKey.Q;
            var dp = PrivKey.DP;
            var dq = PrivKey.DQ;
            var InvQ = PrivKey.InverseQ;


            var arr = new byte[
                mod.Length // 128
                + exp.Length // 3
                + d.Length // 128
                + p.Length // 64
                + q.Length // 64
                + dp.Length // 64
                + dq.Length // 64
                + InvQ.Length // 64
                ];

            int offset = 0;

            Buffer.BlockCopy(mod, 0, arr, offset, mod.Length); //
            offset += mod.Length;
            Buffer.BlockCopy(exp, 0, arr, offset, exp.Length);
            offset += exp.Length;
            Buffer.BlockCopy(d, 0, arr, offset, d.Length);
            offset += d.Length;
            Buffer.BlockCopy(p, 0, arr, offset, p.Length);
            offset += p.Length;
            Buffer.BlockCopy(q, 0, arr, offset, q.Length);
            offset += q.Length;
            Buffer.BlockCopy(dp, 0, arr, offset, dp.Length);
            offset += dp.Length;
            Buffer.BlockCopy(dq, 0, arr, offset, dq.Length);
            offset += dq.Length;
            Buffer.BlockCopy(InvQ, 0, arr, offset, InvQ.Length);
     
   
            return arr;
        }
 
 
        public static byte[] Decrypt(byte[] msg, string InjectedPrivKeyEncoded)
        {
            var pubKey = DeserializePrivKey(Convert.FromBase64String(InjectedPrivKeyEncoded));

            RSACryptoServiceProvider csp = new RSACryptoServiceProvider(RSA_KEY_SIZE);
            csp.ImportParameters(pubKey);

            //apply pkcs#1.5 padding and encrypt our data 
            var decryptedBytes = csp.Decrypt(msg, false);
            //we might want a string representation of our cypher text... base64 will do
            // var cypherText = Convert.ToBase64String(bytesCypherText);
 
            return decryptedBytes;
          
        }
 

        public static byte[] Encrypt(byte[] msg, string InjectedPubKeyEncoded)
        {
            var pubKey = DeserializePubKey(Convert.FromBase64String(InjectedPubKeyEncoded));

            RSACryptoServiceProvider csp = new RSACryptoServiceProvider(RSA_KEY_SIZE);
            csp.ImportParameters(pubKey);

            //apply pkcs#1.5 padding and encrypt our data 
            var bytesCypherText = csp.Encrypt(msg, false);
            //we might want a string representation of our cypher text... base64 will do
            // var cypherText = Convert.ToBase64String(bytesCypherText);

            return bytesCypherText;
        }

        public static byte[] Sha256Hash(byte[] msg)
        {
            SHA256 SHA256 = SHA256.Create();
            return SHA256.ComputeHash(msg);
        }

        public static byte[] Sha1Hash(byte[] msg)
        {
            SHA1 SHA1 = SHA1.Create();
            return SHA1.ComputeHash(msg);
        }

        public static string Sha1Hash(string msg)
        {
            return Convert.ToHexString(Sha1Hash(Encoding.UTF8.GetBytes(msg)));
        }

        public static string Sha256Hash(string msg)
        {
            return Convert.ToHexString(Sha256Hash(Encoding.UTF8.GetBytes(msg)));
        }

        public static byte[] SignBytes(byte[] hash, string base64EncodedPrivKey)
        {
            try
            {
                var privRSAParam = DeserializePrivKey(Convert.FromBase64String(base64EncodedPrivKey));
                // Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
                // to specify the use of SHA1 for hashing.
 
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(RSA_KEY_SIZE);
                rsa.ImportParameters(privRSAParam);
                var modBase64 = Convert.ToBase64String(privRSAParam.Modulus);
                var modHex = ByteArrayToHexString(privRSAParam.Modulus);
                var eBase64 = Convert.ToBase64String(privRSAParam.Exponent);
                var dBase64 = Convert.ToBase64String(privRSAParam.D);

                var pBase64 = Convert.ToBase64String(privRSAParam.P);
                var qBase64 = Convert.ToBase64String(privRSAParam.Q);

                var dpBase64 = Convert.ToBase64String(privRSAParam.DP);
                var dqBase64 = Convert.ToBase64String(privRSAParam.DQ);
                var invqBase64 = Convert.ToBase64String(privRSAParam.InverseQ);

                return rsa.SignHash(hash, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);

                //return rsa.SignData(hash, "SHA1"); // CryptoConfig.MapNameToOID("SHA1"));
                //return RSAalg.SignData(DataStream, new SHA1CryptoServiceProvider());
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public static string ByteArrayToHexString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }


        public static byte[] StringToByteArray(string hex)
        {
            if (hex != null)
            {
                return Enumerable.Range(0, hex.Length)
                                 .Where(x => x % 2 == 0)
                                 .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                                 .ToArray();
            }
            return null;
        }

        public static ISignedMsg SignMsg(ISignedMsg msg, string privateStringBase64Encoded)
        {
            var msgString = msg.stringifyMsg();

            byte[] msgSerializedBytes = Encoding.ASCII.GetBytes(msgString);

            byte[] msgHash = TruRSACrypto.Sha1Hash(msgSerializedBytes);

            string msgHashHex = ByteArrayToHexString(msgHash);

            byte[] byteSig =  TruRSACrypto.SignBytes(msgHash, privateStringBase64Encoded);

            string sigHex = ByteArrayToHexString(byteSig);

            msg.Hash = msgHashHex;
            msg.Signature = sigHex;
 
            return msg;
        }

        public static bool VerifyHash(string msgStr, string SignedDataHex, string addressEnc)
        {
            try
            {
                //Test to see if SigHex is same in code..
                Debug.WriteLine(msgStr);
                byte[] msgBytes = Encoding.ASCII.GetBytes(msgStr);

                var msgHash = TruRSACrypto.Sha1Hash(msgBytes);

                byte[] SignedData = StringToByteArray(SignedDataHex);

                var testStringFromBytes = ByteArrayToHexString(SignedData);

                var hasAsHex = ByteArrayToHexString(msgHash);
                var pubRSAParam = DeserializePubKey(Convert.FromBase64String(addressEnc));
                
                var moduBase64 = Convert.ToBase64String(pubRSAParam.Modulus);
                var expBase64 = Convert.ToBase64String(pubRSAParam.Exponent);
         
                // Create a new instance of RSACryptoServiceProvider using the 
                // key from RSAParameters.
                RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider(RSA_KEY_SIZE);
                RSAalg.ImportParameters(pubRSAParam);

                //var decrypted = RSAalg.DecryptValue(SignedData);
                //string decryptedsigHex = ByteArrayToHexString(decrypted);


                //return decryptedsigHex == hasAsHex;

                // Verify the data using the signature.  Pass a new instance of SHA1CryptoServiceProvider
                // to specify the use of SHA1 for hashing.
                return RSAalg.VerifyHash(msgHash, SignedData, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);

            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return false;
            }

        }

 
        //public  static string Serialize()
        //{
        //    var dict = new Dictionary<string, string>();
        //    dict.Add("PubKey",  AddressEncoded);
        //    dict.Add("PrivKey", PrivateKeyEncoded);

        //    return JsonConvert.SerializeObject(dict);
        //}

        //public static bool Deserialize(string walletData)
        //{
        //    try
        //    {
        //        var wallet = JsonConvert.DeserializeObject<Dictionary<string, string>>(walletData);

        //        PubKey = DeserializePubKey(Convert.FromBase64String(wallet["PubKey"]));
        //        PrivKey = DeserializePrivKey(Convert.FromBase64String(wallet["PrivKey"]));

        //        RSA.ImportParameters(PubKey);
        //        RSA.ImportParameters(PrivKey);

        //        _ = PrivateKeyEncoded;
        //        _ = AddressEncoded;

        //        return true;
        //    }catch(Exception e)
        //    {
        //        return false;
        //    }

        //}
 

    }
}
