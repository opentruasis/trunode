﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 

namespace TruV2Shared
{
    public enum Endianness
    {
        Little,
        Big
    }

 
 
    public static class TruSerializer
    {

            /// <summary>
            /// Take a bunch of SelexType items and serialize them into a new buffer
            /// </summary>
            /// <param name="list"></param>
            /// <returns>a byte buffer if successful, null otherwise</returns>
            public static byte[] Serialize(params ITruMsgBase[] list)
            {
                if (list == null || list.Length == 0) return new byte[0];
                int size = 0;
                foreach (var item in list)
                {
                    if (item != null) size += item.Size;
                }

                byte[] buffer = new byte[size];
                int offset = 0;
                foreach (var item in list)
                {
                    bool success = false;
                    if (item != null)
                    {
                        success = item.Serialize(ref buffer, ref offset);
                    }
                    if (!success) return null;
                }

                return buffer;
            }

            public static byte[] Serialize(IEnumerable<ITruMsgBase> list)
            {
                return Serialize(list.ToArray());
            }

            /// <summary>
            /// Check current platform endianness and swap the byte order of the 4 byte word at the current offset if neccessary.
            /// </summary>
            /// <param name="buffer"></param>
            /// <param name="offset"></param>
            /// <param name="endianness"></param>
            public static void SwapByteOrderIfNeccessary(ref byte[] buffer, int offset, Endianness endianness)
            {
                if (BitConverter.IsLittleEndian && endianness == Endianness.Big || !BitConverter.IsLittleEndian && endianness == Endianness.Little)
                {
                    var temp = buffer[offset];
                    buffer[offset] = buffer[offset + 3];
                    buffer[offset + 3] = temp;
                    temp = buffer[offset + 1];
                    buffer[offset + 1] = buffer[offset + 2];
                    buffer[offset + 2] = temp;
                }
            }


        public static bool Serialize(ref byte[] buffer, ref int offset, bool val)
        {
            if (buffer.Count() - offset < sizeof(bool))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(bool));
            offset += sizeof(bool);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, byte val)
        {
            if (buffer.Count() - offset < sizeof(byte))
            {
                return false;
            }
            buffer[offset] = val;
            offset += sizeof(byte);

            return true;
        }
        public static bool Serialize(ref byte[] buffer, ref int offset, char val)
        {
            if (buffer.Count() - offset < sizeof(char))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(char));
            offset += sizeof(char);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, ushort val)
        {
            if (buffer.Count() - offset < sizeof(ushort))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(ushort));
            offset += sizeof(ushort);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, short val)
        {
            if (buffer.Count() - offset < sizeof(short))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(short));
            offset += sizeof(short);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, uint val)
        {
            if (buffer.Count() - offset < sizeof(uint))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(uint));
            offset += sizeof(uint);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, int val)
        {
            if (buffer.Count() - offset < sizeof(int))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(int));
            offset += sizeof(int);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, UInt64 val)
        {
            if (buffer.Count() - offset < sizeof(UInt64))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(UInt64));
            offset += sizeof(UInt64);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, Int64 val)
        {
            if (buffer.Count() - offset < sizeof(Int64))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(Int64));
            offset += sizeof(Int64);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, float val)
        {
            if (buffer.Count() - offset < sizeof(float))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(float));
            offset += sizeof(float);

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, double val)
        {
            if (buffer.Count() - offset < sizeof(double))
            {
                return false;
            }

            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, buffer, offset, sizeof(double));
            offset += sizeof(double);

            return true;
        }

        public static bool SerializeVariableLength(ref byte[] buffer, ref int offset, string val)        // variable sized string
        {
            UInt32 stringSizeUnsigned = (UInt32)val.Length;
            Int32 stringSizeSigned = (Int32)stringSizeUnsigned;

            if (!Serialize(ref buffer, ref offset, stringSizeUnsigned))
                return false;

            if (stringSizeUnsigned == 0)
                return true;

            if (buffer.Count() - offset < stringSizeUnsigned)
                return false;

            Buffer.BlockCopy(ASCIIEncoding.ASCII.GetBytes(val), 0, buffer, offset, stringSizeSigned);
            offset += stringSizeSigned;

            return true;
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, string val)         // static length string
        {
            return TruSerializer.Serialize(ref buffer, ref offset, val, ASCIIEncoding.ASCII);
        }

        public static bool Serialize(ref byte[] buffer, ref int offset, string val, Encoding enc)
        {
            if (buffer.Count() - offset < val.Length)
            {
                return false;
            }

            Buffer.BlockCopy(enc.GetBytes(val), 0, buffer, offset, val.Length);
            offset += val.Length;

            return true;
        }


        public static bool Serialize(ref byte[] buffer, ref int offset,  byte[] enc)
        {
            if (buffer.Count() - offset < enc.Length)
            {
                return false;
            }

            Buffer.BlockCopy(enc, 0, buffer, offset, enc.Length);
            offset += enc.Length;

            return true;
        }

        public static bool Serialize<TEnum>(ref byte[] buffer, ref int offset, TEnum val) where TEnum : struct, IConvertible
        {
            Type t = Enum.GetUnderlyingType(typeof(TEnum));
            bool success = true;

            TypeCode tc = Type.GetTypeCode(t);

            switch (tc)
            {
                case TypeCode.Byte:
                    {
                        byte uVal = (byte)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.UInt16:
                    {
                        ushort uVal = (ushort)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.UInt32:
                    {
                        uint uVal = (uint)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.UInt64:
                    {
                        UInt64 uVal = (UInt64)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.Char:
                    {
                        char uVal = (char)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.Int16:
                    {
                        short uVal = (short)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.Int32:
                    {
                        int uVal = (int)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
                case TypeCode.Int64:
                    {
                        Int64 uVal = (Int64)Convert.ChangeType(val, tc);
                        success = TruSerializer.Serialize(ref buffer, ref offset, uVal);
                        break;
                    }
            }

            return success;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref bool val)
        {
            if (buffer.Count() - offset < sizeof(bool))
            {
                return false;
            }

            val = BitConverter.ToBoolean(buffer, offset);
            offset += sizeof(bool);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref byte val)
        {
            if (buffer.Count() - offset < sizeof(byte))
            {
                return false;
            }

            val = buffer[offset];
            offset += sizeof(byte);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref char val)
        {
            if (buffer.Count() - offset < sizeof(char))
            {
                return false;
            }

            val = BitConverter.ToChar(buffer, offset);
            offset += sizeof(char);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref ushort val)
        {
            if (buffer.Count() - offset < sizeof(ushort))
            {
                return false;
            }

            val = BitConverter.ToUInt16(buffer, offset);
            offset += sizeof(ushort);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref short val)
        {
            if (buffer.Count() - offset < sizeof(short))
            {
                return false;
            }

            val = BitConverter.ToInt16(buffer, offset);
            offset += sizeof(short);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref uint val)
        {
            if (buffer.Count() - offset < sizeof(uint))
            {
                return false;
            }

            val = BitConverter.ToUInt32(buffer, offset);
            offset += sizeof(uint);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref int val)
        {
            if (buffer.Count() - offset < sizeof(int))
            {
                return false;
            }

            val = BitConverter.ToInt32(buffer, offset);
            offset += sizeof(int);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref UInt64 val)
        {
            if (buffer.Count() - offset < sizeof(UInt64))
            {
                return false;
            }

            val = BitConverter.ToUInt64(buffer, offset);
            offset += sizeof(UInt64);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref Int64 val)
        {
            if (buffer.Count() - offset < sizeof(Int64))
            {
                return false;
            }

            val = BitConverter.ToInt64(buffer, offset);
            offset += sizeof(Int64);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref float val)
        {
            if (buffer.Count() - offset < sizeof(float))
            {
                return false;
            }

            val = BitConverter.ToSingle(buffer, offset);
            offset += sizeof(float);

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref double val)
        {
            if (buffer.Count() - offset < sizeof(double))
            {
                return false;
            }

            val = BitConverter.ToDouble(buffer, offset);
            offset += sizeof(double);

            return true;
        }

        public static bool DeserializeVariableLength(ref byte[] buffer, ref int offset, ref string val)       // variable sized strings (<256 chars)
        {
            UInt32 stringSizeUnsigned = 0;
            if (!Deserialize(ref buffer, ref offset, ref stringSizeUnsigned))
                return false;

            Int32 stringSizeSigned = (Int32)stringSizeUnsigned;
            if (stringSizeUnsigned == 0)
                return true;

            if (buffer.Count() - offset < stringSizeUnsigned)
                return false;

            val = BitConverter.ToString(buffer, offset, stringSizeSigned);
            offset += stringSizeSigned;

            return true;
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref string val, int length)
        {
            return TruSerializer.Deserialize(ref buffer, ref offset, ref val, length, ASCIIEncoding.ASCII);
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref string val, uint length)
        {
            return TruSerializer.Deserialize(ref buffer, ref offset, ref val, (int)length, ASCIIEncoding.ASCII);
        }

        public static bool Deserialize(ref byte[] buffer, ref int offset, ref string val, int length, Encoding enc)
        {
            if ((buffer.Count() - offset < length) ||
                length < 0)
            {
                return false;
            }

            val = enc.GetString(buffer, offset, length);
            offset += length;

            return true;
        }
        public static bool Deserialize<TEnum>(ref byte[] buffer, ref int offset, ref TEnum val) where TEnum : struct, IConvertible
        {
            Type t = Enum.GetUnderlyingType(typeof(TEnum));
            bool success = true;

            switch (Type.GetTypeCode(t))
            {
                case TypeCode.Byte:
                    {
                        byte uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.UInt16:
                    {
                        ushort uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.UInt32:
                    {
                        uint uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.UInt64:
                    {
                        UInt64 uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.Char:
                    {
                        char uVal = (char)0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.Int16:
                    {
                        short uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.Int32:
                    {
                        int uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
                case TypeCode.Int64:
                    {
                        Int64 uVal = 0;
                        success = TruSerializer.Deserialize(ref buffer, ref offset, ref uVal);
                        val = (TEnum)Enum.ToObject(typeof(TEnum), uVal);
                        break;
                    }
            }

            return success;
        }


        public static bool Deserialize(ref byte[] buffer, ref int offset, int Length, ref byte[] val) 
        {
            if(val == null)
            {
                return false;
            }
            if ((buffer.Count() - offset < val.Length) ||
               val.Length < 0)
            {
                return false;
            }

            Buffer.BlockCopy(buffer, offset, val, 0, Length);
            offset += Length;
            return true;
        }

    }
}
