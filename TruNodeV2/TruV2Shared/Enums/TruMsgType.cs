﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TruV2Shared
{
    public enum TruMsgType : byte
    {
        None,
        Version,
        VersionAck,
        Ack,
        ClientInfo, //X
        ClientHeartBeat, //X
        SuperHeartBeat,
        SuperHeartBeatResp,
        LeaderVote,
        LeadersChosen,
        NetWalletsDTO,
        ChainData,
        SuperURLList,
        Req, //X
        SubComponent, 
        SuperNodeInfoList, //X
        SuperNodeInfo, //X
        TransferWalletMsg, //X
        IDAvailCheck, //X
        Response, //X
        NewNodeJoinedTruNet, //X
        NodeInfoUpdate, //X
        TruMsgChunk, //X
        NodeInfoList, //X
        Payment, //X
        PaymentValidation, //X
        BalanceUpdate, //X
        TransactionValidationResponseList,
        WalletNeedsUpdateListMsg, //X
        NewSuperNodeLeader //

    }

}
