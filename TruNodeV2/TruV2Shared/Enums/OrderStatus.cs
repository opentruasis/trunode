﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Enums
{
    public enum OrderStatus: byte
    {
        None = 0,
        Initiated,
        Paid,
        Fullfilled,
        Canceled
    }
}
