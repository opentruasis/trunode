﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared.Enums
{
    public enum PaymentValidationResp: byte
    {
        NotEnoughFunds,
        Valid,
        UnknownPayer,
        PubKeyMismatch
    }
}
