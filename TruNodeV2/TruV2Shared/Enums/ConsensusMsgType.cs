﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruSharedV2
{
    public enum ConsensusMsgType
    {
        PRE_PREPARE,
        PREPARE,
        COMMIT,
        ROUND_CHANGE,
        LEADER_VOTE,
        LEADER_CHOSEN,
        VOTE_RESTART
    }
}
