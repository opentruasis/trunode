﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared.Enums
{
    public enum TruAckType : byte
    {
        //Client SIde Request Types.
        None,
        Heartbeat,
        SuperPaymentAck,
        SuperSentBalanceUpdateToClients,
        LeaderInitiation,
        getWalletInfoByIdAck,
        PaymentFailed,
        PaymentSucceeded,
        AdResultGood,
        AdResultBad,
        JobResultGood,
        JobResultBad,
        GetCurrentSuperNodeLeader
    }
}
