﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TruExchange.Enums
{
    public enum OrderType: byte
    {
        Buy = 0,
        Sell
    }
}
