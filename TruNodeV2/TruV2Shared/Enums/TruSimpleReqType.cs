﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared.Enums
{
    public enum TruSimpleReqType : byte
    {
        //Client SIde Request Types.
        IsIDAvailable,
        NewNodeAddedToNet,
        GetNodeList,
        SearchUsers,
        SearchUsersById,
        ValidatePayment,
        PaymentRequest,

        GetRunningAds,
        GetJobTypes,
        GetAvailableJobs,

        PayTruForWatchingAds,   
        PayTruForProcessingJob,


        GetWalletInfoById,
        GetWalletInfoForConsensus,
        MajorTrustFail,

        // Super Node Request Types.
        ReqSuperNdList,
        TransferWalletSuperNotification,
        ClientInfoUpdate,
        InitializeLeaderElection        
    }
}
