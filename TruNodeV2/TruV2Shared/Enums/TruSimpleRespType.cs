﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared.Enums
{
    public enum TruSimpleRespType : byte
    {
        boolFalse,
        boolTrue,
        boolAccessDenied,
        data,
        consensusNodeInfo,
        noNodeInDB,
        paymentSuccess,
        paymentFailed,
        searchUserCount
    }
}
