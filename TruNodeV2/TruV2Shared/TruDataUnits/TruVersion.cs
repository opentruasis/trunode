﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.DTOs;

namespace TruV2Shared.TruDataUnits
{
    public class TruVersion : TruDTO
    {

        public TruMsgType Type { get => TruMsgType.Version; set { } }
        public string SenderId { get; set; }
        public string ConnectionID { get; set; }
 
        public string Version { get; set; }
        public SuperNodeInfo SuperNodeInfo { get; set; }

    }
}
