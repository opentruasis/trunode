﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class SuperUrlManager
    {
        public SuperUrlManager(string seedUrl, int index = 0)
        {
            PersistPath = PersistPath + index;
            EnsurePathExists();
            EnsureFileExists(SuperURLfile);
            LoadUrls();
            Add(seedUrl);
        }

        public object _lockObj = new object();

        private void EnsurePathExists()
        {
            string dbPathString = "";

            try
            {
                // Determine whether the directory exists.
                if (!Directory.Exists(PersistPath))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PersistPath);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(PersistPath));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

        }
        private void EnsureFileExists(string file)
        {
            if(!File.Exists(Path.Combine(PersistPath, SuperURLfile)))
            {
                File.Create(Path.Combine(PersistPath, SuperURLfile));
            }
        }

        private bool isLoaded = false;
         
        private List<string> listOfUrls = new List<string>();

        private string PersistPath = "SuperUrls_";

        private const string SuperURLfile = "superUrls.json";


        public List<string> URLS
        {
            get
            {
                if (!isLoaded)
                {
                    LoadUrls();
                }
                return listOfUrls;
            }
        }

        public int Count
        {
            get
            {
                if (!isLoaded)
                {
                    LoadUrls();
                }
                return listOfUrls.Count;
            }
        }

     

        public void Add(string url)
        {
            bool didChange = false;
            if (!listOfUrls.Contains(url))
            {
                listOfUrls.Add(url);
                didChange = true;
            }
            if (didChange)
            {
                Persist();
            }
        }

 
        public void Remove(string url)
        {
            bool didChange = false;
            if (listOfUrls.Contains(url))
            {
                listOfUrls.Remove(url);
                didChange = true;
            }
            if (didChange)
            {
                Persist();
            }
        }


        private void Persist() {
            ///Write to file.
            try
            {
                File.WriteAllText(Path.Combine(PersistPath, SuperURLfile), JsonConvert.SerializeObject(listOfUrls));
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        private void LoadUrls()
        {
            try
            {
                isLoaded = true;
                var loadedUrls = File.ReadAllText(Path.Combine(PersistPath, SuperURLfile));
                List<string> urls = JsonConvert.DeserializeObject<List<string>>(loadedUrls);
                if (urls != null)
                {
                    foreach (var url in urls)
                    {
                        if (!listOfUrls.Contains(url))
                        {
                            listOfUrls.Add(url);
                        }
                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
