﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruWallet 
    {
  
        public TruWallet()
        {
            ID = Guid.NewGuid().ToString();
            Keys = TruRSACrypto.GenerateNewKeys();
        }
 
        public string ID { get; set; } =  "";
        public WalletKeys Keys { get; set; }
        public string Handle { get; set; }
        public string FullName { get; set; }
        public string PushNotifToken { get; set; }
        public string ProfilePicBase64 { get; set; }
        public double Balance { get; set; } = 0;
  
    }
}
