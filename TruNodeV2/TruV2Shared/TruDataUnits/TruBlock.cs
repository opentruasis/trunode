﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruBlock : SignedMsg
    {

        public TruBlock() { }

        public TruBlock(long id, byte[] prevHash)
        {
            ID = id;
            PreviousHashBytes = prevHash;
        }

        public byte[] PreviousHashBytes = new byte[0];

        public string PreviousHash
        {
            get
            {
                if (PreviousHashBytes != null)
                {
                    return TruRSACrypto.ByteArrayToHexString(PreviousHashBytes);
                }
                else
                {
                    return "";
                }
            }
        }

        public long ID { get; set; } = 0;

        private List<TruTx> _transactions = new List<TruTx>();

        public List<TruTx> Transactions => _transactions;
             
        public int ExecutedAt { get; set; } //Timestamp when block appended to chain.

        public int BlockWeight { get // Returns the size of the block in bytes.. Heaviest wins.
            {
                int txBytes = 0;
                Transactions.ForEach(t => txBytes += t.TxWeight);
                return sizeof(long) + PreviousHashBytes.Length + txBytes + BlockHashByte.Length;
            } 
        }

        public string BlockHash
        {
            get
            {
                if (BlockHashByte != null)
                {
                    return TruRSACrypto.ByteArrayToHexString(BlockHashByte);
                }
                else
                {
                    return "";
                }
            }
        }

        public byte[] BlockHashByte
        {
            get
            {
                try
                {
                    List<byte[]> collectBytes = new List<byte[]>();
                    int totalBytes = 0;

                    //previousHash
                    totalBytes += PreviousHashBytes.Length;
                    collectBytes.Add(PreviousHashBytes);

                    foreach (var tx in _transactions)
                    {
                        var IdBytes = BitConverter.GetBytes(ID);
                        var truAmountBytes = BitConverter.GetBytes(tx.TruAmount);
                        var signerBytes = Encoding.ASCII.GetBytes(tx.SignerId);
                        var receiverBytes = Encoding.ASCII.GetBytes(tx.ReceiverId);

                        collectBytes.Add(IdBytes);
                        collectBytes.Add(truAmountBytes);
                        collectBytes.Add(signerBytes);
                        collectBytes.Add(receiverBytes);
                        totalBytes += IdBytes.Length;
                        totalBytes += truAmountBytes.Length;
                        totalBytes += signerBytes.Length;
                        totalBytes += receiverBytes.Length;
                    }

                    byte[] bytes = new byte[totalBytes];

                    int offset = 0;
                    foreach (var b in collectBytes)
                    {
                        Buffer.BlockCopy(b, 0, bytes, offset, b.Length);
                    }


                    HashAlgorithm sha = SHA256.Create();
                    byte[] result = sha.ComputeHash(bytes);
                    return result;
                }catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    return new byte[0];
                }
            }
        }



        public TruTx TxAt(string txId)
        {
            return _transactions.FirstOrDefault(t => t.ID == txId);
        }


        public bool AddTx(TruTx newTx, bool isBlockZero = false)
        {
            if (newTx.isVerified || isBlockZero)
            {
                try
                {
                    _transactions.Add(newTx);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            return false;
        }
 

        public override string stringifyMsg()
        {
            return "";
        }
    }
}
