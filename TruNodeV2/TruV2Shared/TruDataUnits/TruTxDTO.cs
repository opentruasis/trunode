﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruTxDTO
    {
        public  string ID { get; set; }

        public double TruAmount { get; set; }
 
        public string ReceiverId { get; set; }

        public string ReceiverPubKey { get; set; }

        public string SignerId { get; set; }
 
        public string Signature { get; set; }

        public string Hash { get; set; }
    }
}
