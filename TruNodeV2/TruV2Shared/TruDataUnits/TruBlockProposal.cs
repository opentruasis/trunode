﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruSharedV2;
using TruV2Shared.Interfaces;
using TruV2Shared.TruDataUnits;

namespace TruV2Shared.TruDataUnits
{
    public class TruBlockProposal : TruConsensusMsg
    {
        public TruBlockProposal()
        {
            MsgType = ConsensusMsgType.PRE_PREPARE;
        }
        public TruBlock Block { get; set; }
    
        public override string stringifyMsg()
        {
            return MsgType.ToString() + "--" + Block.stringifyMsg();
        }
    }
}
