﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class LeaderElectRestart : TruConsensusMsg
    {
        public LeaderElectRestart()
        {
            MsgType = TruSharedV2.ConsensusMsgType.VOTE_RESTART;
        }
  

        public override string stringifyMsg()
        {
            return MsgType + "--";
        }
    }
}
