﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.Interfaces;
 

namespace TruV2Shared.TruDataUnits
{
    public class TruTx : SignedMsg
    {
        public string ID { get; set; }  = Guid.NewGuid().ToString();

        public bool isVerified { get => true; }// Verify(); }

        public double TruAmount = 0;

        public string ReceiverId = "";

        public string ReceiverPubKey = "";

        public byte[] Blob = new byte[0]; // Persisting data for DAPS// maximum bytes.? 10 MB??? who knows..

        public TruToken TruToken { get; set; }

        public TruApp TruDap { get; set; }

        public string CreatedOn { get; set; }

  
        public int TxWeight { get // Returns tx byte size:
            {
                return Encoding.ASCII.GetByteCount(stringifyMsg());
            } 
        }

        public override string stringifyMsg()
        {
            var truAmount = this.TruAmount.ToString("N6").Replace(",", "");
            var txStr = JsonConvert.SerializeObject(new Dictionary<string, dynamic>{
                {"ID", ID },
                {"TruAmount", truAmount },
                {"SignerId", this.SignerId },
                {"ReceiverId", this.ReceiverId },
                {"ReceiverPubKey", this.ReceiverPubKey }
            });

            return txStr;
        }


        public List<TruTxVerify> Verifications = new List<TruTxVerify>();
        
        
        public TruTx(){}

        public TruTx(TruTxDTO newTxFromClient)
        {
            ID = newTxFromClient.ID;
            TruAmount = newTxFromClient.TruAmount;
            SignerId = newTxFromClient.SignerId;
            Signature = newTxFromClient.Signature;
            Hash = newTxFromClient.Hash;
            ReceiverId = newTxFromClient.ReceiverId;
            ReceiverPubKey = newTxFromClient.ReceiverPubKey;
        }


        public int ExpectedValidationResponses { get; set; }

        public TruTx Copy()
        {
            return new TruTx()
            {
                ID = ID,
                TruAmount = TruAmount,
                SignerId = SignerId,
                Signature = Signature,
                Hash = Hash,
                ReceiverId = ReceiverId,
                ReceiverPubKey = ReceiverPubKey,
            };
        }

        public void AddVerification(TruTxVerify txVerify)
        {
            Verifications.Add(txVerify);
        }

        public bool Verify()
        {
            var goodCount = 0;
            foreach(var txv in Verifications)
            {
                if (txv.SenderBalance >= this.TruAmount)
                {
                    goodCount++;
                }
            }
 
            if (goodCount >= 0.9 * ExpectedValidationResponses)
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }


        public override bool isValidSig(string injectedPubKeyBase64Enc)
        {
            try
            {
              var msgString = this.stringifyMsg();
              return TruRSACrypto.VerifyHash(
                     msgString,
                     this.Signature,
                     injectedPubKeyBase64Enc
                     );

            }catch(Exception e)
            {
                return false;
            }
        }
 
    }
}
