﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TruV2Shared.TruDataUnits;

namespace TruV2Shared.TruDataUnits
{

    public class TruLedger
    {

        private string PersistPath = "TruLedger_";
 
        private SortedDictionary<long, TruBlock> _chain = new SortedDictionary<long, TruBlock>();

        public SortedDictionary<long, TruBlock> Chain => _chain;
       
        private Dictionary<string, double> _balances = new Dictionary<string, double>();

        public Dictionary<string, double> Balances => GetLatestBalances();

        public long EndBlockId => (long)(_chain.Count > 0 ? _chain.Count - 1 : 0);

        private TruWalletManager _networkWallets;

        public TruBlock EndBlock => _chain[EndBlockId];

        public long CrawledTo { get; set; } = 0;

        public TruLedger(TruWalletManager netwallets, int index = 0) {  //index here used to test and make multiple ledgers.
            PersistPath = PersistPath + index;
            EnsurePathExists();
            _networkWallets = netwallets;
        }

        public TruLedger(TruWallet truasisWallet, TruWalletManager netwallets, int index = 0) //index here used to test and make multiple ledgers.
        {
            PersistPath = PersistPath + index;
            EnsurePathExists();
            _networkWallets = netwallets;
            var blockZero = new TruBlock(0, Array.Empty<byte>());

            var firstTx = new TruTx()
            {
                ReceiverId = truasisWallet.ID,
                ReceiverPubKey = truasisWallet.Keys.PubKey,
                TruAmount = 100000,
                SignerId = ""
            };


            Console.WriteLine("First TX::::");
            Console.WriteLine(firstTx);
            firstTx = TruRSACrypto.SignMsg(firstTx, truasisWallet.Keys.PrivKey) as TruTx;

            var msgSig = firstTx.isValidSig(truasisWallet.Keys.PubKey);

            
            Console.WriteLine(firstTx);

            blockZero.AddTx(firstTx, true);
 

            _chain.Add(blockZero.ID, blockZero);
 
            AppendChain();
    
            var initialBalances = CrawlChain();

            Console.WriteLine(initialBalances);
        }



        private void EnsurePathExists()
        {
            try
            {
                // Determine whether the directory exists.
                if (!Directory.Exists(PersistPath))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PersistPath);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(PersistPath));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }

        public void PersistAll()
        {
            //Check if folder exists.. if not make it..
            string[] paths = Directory.GetFiles(PersistPath);

            string[] files = new string[paths.Length];
            for (int i = 0; i<paths.Length; i++)
            {
                var p = paths[i];
                Console.WriteLine(p);
                var file = Path.GetFileName(p);
                files[i] = file;
                //Make sure lists from 0 to X
            }

            if (ValidChain)
            {
                foreach (var (id, b) in Chain)
                {
                    ///Write to file.
                    File.WriteAllText(Path.Combine(PersistPath, id.ToString()), JsonConvert.SerializeObject(b, Formatting.Indented));
                }
            }
        }


        private bool validTransactions(List<TruTx> transactions)
        {
            var isValid = true;
            isValid &= transactions.Count > 0;
            foreach (var tx in transactions)
            {
                if (this._networkWallets.ContainsKey(tx.SignerId))
                {
                    //Signer of transaction..
                    var pubKeyInMyRoledex = this._networkWallets[tx.SignerId].ID;
                    isValid &= tx.isValidSig(pubKeyInMyRoledex);
                    isValid &= tx.isVerified;
                }
                else
                {
                    isValid = false;
                }
                if (!isValid) return false;
            }

            return isValid;
        }
 
        public bool IsValidBlock(TruBlock block)
        {
            var currentLastBlock = EndBlock;
            if (
              currentLastBlock.ID + 1 == block.ID &&
              block.PreviousHash == currentLastBlock.BlockHash &&
              this.validTransactions(block.Transactions)
            )
            {
               Console.WriteLine("BLOCK VALID");
                return true;
            }
            else
            {
                Console.WriteLine("BLOCK INVLAID");
                return false;
            }


        }



        public bool ValidChain
        {
            get
            {
                var lastHash = "";
                var chainComplete = true;
                foreach (var (id, b) in Chain)
                {
                    if (id == 0)
                    {
                        lastHash = b.BlockHash;
                    }
                    else
                    {
                        if (b.PreviousHash == lastHash)
                        {
                            lastHash = b.BlockHash;
                        }
                        else
                        {
                            Console.WriteLine("Comlete TO: " + (id-1));
                            chainComplete = false;
                            break;
                        }
                    }
                }

                return chainComplete;
            }
        }


        public string GetCurrentBlockHash()
        {
            if (Chain.ContainsKey(EndBlockId)) {
                return Chain[EndBlockId].PreviousHash;
            }
            return "";
        }

        public void PersistBlock(long block)
        {
            ///Write to file.
            File.WriteAllText(Path.Combine(PersistPath, block.ToString()), JsonConvert.SerializeObject(Chain[block], Formatting.Indented));
        }


        public void LoadChain()
        {
            string[] paths = Directory.GetFiles(PersistPath);
            foreach (var block in paths)
            {
                var loadedBlock = File.ReadAllText(block);
                TruBlock truBlock = JsonConvert.DeserializeObject<TruBlock>(loadedBlock);
                if (Chain.ContainsKey(truBlock.ID))
                {
                    Chain[truBlock.ID] = truBlock;
                }
                else
                {
                    _chain.Add(truBlock.ID, truBlock);
                } 
            }
        }

        public void ClearPersistance()
        {
            DirectoryInfo di = new DirectoryInfo(PersistPath);
            FileInfo[] files = di.GetFiles();
            foreach (FileInfo file in files)
            {
                file.Delete();
            }
        }

        public bool isNewNode
        {
            get
            {
               string[] paths = Directory.GetFiles(PersistPath);
               return paths.Length < 1;
            }
        }



        public bool AppendChain()
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)t.TotalSeconds;
            Console.WriteLine(secondsSinceEpoch);
            _chain[EndBlockId].ExecutedAt = secondsSinceEpoch;
            PersistBlock(EndBlockId);  // Save completed block..
            TruBlock nextBlock = new TruBlock(EndBlockId + 1, _chain[EndBlockId].BlockHashByte);
            _chain.Add(nextBlock.ID, nextBlock);
            //PersistBlock(EndBlockId);  // Save new Started Block..
            // RestartBlockTimer();
         
            return true;
        }

        public bool AddNewTx(TruTx tx)
        {
            if (_chain[EndBlockId].TxAt(tx.ID) == null)
            { 
                return _chain[EndBlockId].AddTx(tx);
            }
            return false;
        }


        public Dictionary<string, double> GetLatestBalances()
        {
            return BalancesWithLastBlock(CrawlChain());
        }

        public Dictionary<string, double> BalancesWithLastBlock(Dictionary<string, double> balances)
        {
            var lastBlock = _chain[EndBlockId];
            LoopTransactions(balances, lastBlock);
            return balances;
        }

        public long LedgerSize { get // Returns the file size in bytes (Not size on disk).
            {
                long chainSize = 0;
                string[] paths = Directory.GetFiles(PersistPath);

                string[] files = new string[paths.Length];
                for (int i = 0; i < paths.Length; i++)
                {
                    var p = paths[i];

                    Console.WriteLine(p);
                    FileInfo fileInfo = new FileInfo(p);
                    chainSize += fileInfo.Length;
                    //Make sure lists from 0 to X
                }
 
                return chainSize;
            } 
        }



        private void LoopTransactions(Dictionary<string, double> blances, TruBlock block)
        {
            foreach (var nextTx in block.Transactions)
            {
                if (blances.ContainsKey(nextTx.SignerId))
                {
                    blances[nextTx.SignerId] -= nextTx.TruAmount;
                }
                else
                {
                    if (block.ID != 0)
                    {
                        throw new Exception("Unkown Sender tried to send TRU");
                    }
                }


                if (blances.ContainsKey(nextTx.ReceiverId))
                {
                    blances[nextTx.ReceiverId] += nextTx.TruAmount;
                }
                else
                {
                    blances.Add(nextTx.ReceiverId, nextTx.TruAmount);
                }

            }

        }

        public Dictionary<string, double> CrawlChain(Dictionary<string, double> balances = null)
        {
            Console.WriteLine("Block Size: " + EndBlockId);
            if (CrawledTo > (long)EndBlockId-1) // What we are doing is cacheing the crawl.. 
            {
                var cachedDictionary = new Dictionary<string, double>(_balances);
                return cachedDictionary;  // Balances is always up to but not including last block. 
            }

            if (balances == null)
            {
                balances = _balances;
            }

            var nextTruBalances = new Dictionary<string, double>(balances);

            TruBlock nextBlock = _chain[(CrawledTo)];
 
            while (nextBlock != null)
            {
                LoopTransactions(nextTruBalances, nextBlock);
                CrawledTo++;

                if (CrawledTo <= EndBlockId - 1)
                {
                    
                    if (_chain.ContainsKey(CrawledTo))
                    {

                        nextBlock = _chain[CrawledTo];
                    }
                    else
                    {
                        nextBlock = null;
                    }
                }
                else
                {
                    nextBlock = null;
                }
            }

            _balances = nextTruBalances;
            return nextTruBalances;
         
        }


        public double GetBalanceAt(string walletId)
        {
            var balanceDict = CrawlChain(this.Balances);

            balanceDict = BalancesWithLastBlock(balanceDict);

            if (balanceDict.ContainsKey(walletId))
            {
                return balanceDict[walletId];
            }
            return 0;
        }



        public SortedDictionary<long, TruBlock> GetLedgerRunnout(long blockId)
        {
            var remainingRunnout =  new SortedDictionary<long, TruBlock>();

            var nextBlock = _chain[blockId];
 
            while(nextBlock != null)
            {
                remainingRunnout.Add(nextBlock.ID, nextBlock);
                if (_chain.ContainsKey(nextBlock.ID + 1))
                {
                    nextBlock = _chain[nextBlock.ID + 1];
                }
                else
                {
                    nextBlock = null;
                }
            }

            return remainingRunnout;
        }



        public SortedDictionary<long, TruBlock> GetSomeLedger(long blockId, int maxBlocksToSend = 10)
        {
            var remainingRunnout = new SortedDictionary<long, TruBlock>();
            if (blockId < 0) blockId = 0;
            var nextBlock = _chain[blockId];

            while (nextBlock != null && remainingRunnout.Count < maxBlocksToSend)
            {
                if (remainingRunnout.ContainsKey(nextBlock.ID))
                {
                    remainingRunnout[nextBlock.ID] = nextBlock;
                }
                else
                {
                    remainingRunnout.Add(nextBlock.ID, nextBlock);
                }
                if (_chain.ContainsKey(nextBlock.ID + 1))
                {
                    nextBlock = _chain[nextBlock.ID + 1];
                }
                else
                {
                    nextBlock = null;
                }
            }

            return remainingRunnout;
        }


    }
}
