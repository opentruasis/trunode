﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.DTOs;

namespace TruV2Shared.TruDataUnits
{
    public class TruVersionAck : TruDTO
    {
        public string Version { get; set; }

        public SuperNodeInfo SuperNodeInfo { get; set; }
 
        public TruMsgType Type { get => TruMsgType.VersionAck; set { } }
        public string SenderId { get; set; }
        public string ConnectionID { get; set; }
    }
}
