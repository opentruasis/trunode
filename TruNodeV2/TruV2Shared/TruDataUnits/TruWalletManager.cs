﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruWalletManager
    {

        public TruWalletManager(int index)
        {
            PersistPath = PersistPath + index;
            EnsurePathExists();
            LoadAllWallets();
        }


        private void EnsurePathExists()
        {
            string dbPathString = "";

            try
            {
                // Determine whether the directory exists.
                if (!Directory.Exists(PersistPath))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PersistPath);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(PersistPath));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

        }


        private Dictionary<string, TruWallet> listOfWallets = new Dictionary<string, TruWallet>();

        private string PersistPath = "TruWallets_";


        public Dictionary<string, TruWallet> Wallets
        {
            get
            {
                LoadAllWallets();
                return listOfWallets;
            }
        }

        public int Count
        {
            get
            {
                return CountOnFile();
            }
        }

        public TruWallet this[string walletId]
        {
            get
            {
                if (ContainsKey(walletId))
                {
                    if (listOfWallets.ContainsKey(walletId))
                    {
                        return listOfWallets[walletId];
                    }

                    if (IsOnDisk(walletId))
                    {
                        return ReadWallet(walletId);
                    }
                }
                return null;
            }

            set
            {
                if (listOfWallets.ContainsKey(walletId))
                {
                        listOfWallets[walletId] = value;
                }
                SaveWallet(value);
            }
        }

        public TruWallet FindByPubKey(string key)
        {
            bool walletExists = false;
            var wallet = listOfWallets.Values.FirstOrDefault((w) =>
            {
                if (w.Keys.PubKey == key)
                {
                    walletExists = true;
                    return true;
                }
                else
                {
                    return false;
                }
            });

            return wallet;
        }


        private int CountOnFile()
        {
            string[] paths = Directory.GetFiles(PersistPath);
            return paths.Length;
        }

        public void Add(string id, TruWallet wallet)
        {
 
            Persist(wallet);
            if (listOfWallets.ContainsKey(id))
            {
                listOfWallets[id] = wallet;
            }
            else
            {
                listOfWallets.Add(id, wallet);
            }

        }
        public void Append(Dictionary<string, TruWallet> wallets)
        {
            foreach(var (walletId, wallet) in wallets)
            {
                Add(walletId, wallet);
            }
        }
        

        public bool ContainsKey(string id)
        {
           var inRam = listOfWallets.ContainsKey(id);
           var onDisk = IsOnDisk(id);

           return inRam || onDisk;
        }


        private void Persist(TruWallet wallet) {
            ///Write to file.
            File.WriteAllText(Path.Combine(PersistPath, wallet.ID), JsonConvert.SerializeObject(wallet, Formatting.Indented));
        }


        private bool IsOnDisk(string walletId)
        {
            return File.Exists(Path.Combine(PersistPath, walletId));
        }
        

        private TruWallet ReadWallet(string walletId)
        {
            var loadedWallet = File.ReadAllText(Path.Combine(PersistPath, walletId));
            TruWallet truWallet = JsonConvert.DeserializeObject<TruWallet>(loadedWallet);

            if (listOfWallets.ContainsKey(walletId))
            {
                listOfWallets[walletId] = truWallet;
            }
            else
            {
                listOfWallets.Add(walletId, truWallet);
            }

            return truWallet;
        }

        private void SaveWallet(TruWallet wallet)
        {
            Persist(wallet);
        }


        private void LoadAllWallets()
        {
            if(listOfWallets.Count != Count)
            {
                string[] paths = Directory.GetFiles(PersistPath);
                foreach (var wallet in paths)
                {
                    var loadedWallet = File.ReadAllText(wallet);
                    TruWallet truWallet = JsonConvert.DeserializeObject<TruWallet>(loadedWallet);
                    if (listOfWallets.ContainsKey(truWallet.ID))
                    {
                        listOfWallets[truWallet.ID] = truWallet;
                    }
                    else
                    {
                        listOfWallets.Add(truWallet.ID, truWallet);
                    }
                }
            }
        }

        public List<TruWallet> Search(string searchString)
        {
            List<TruWallet> filtered = Wallets.Values.ToList().FindAll((w) =>
            {
                if (w.Keys.PubKey == searchString)
                {
                    return true;
                }
                if (w.FullName != null && w.Handle != null)
                {
                    if (w.FullName.Contains(searchString) || w.Handle.Contains(searchString))
                    {
                        return true;
                    }
                }
                return false;

            });

            return filtered;
        }

   

    }
}
