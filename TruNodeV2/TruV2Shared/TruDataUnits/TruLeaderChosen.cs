﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruLeaderChosen : TruConsensusMsg
    {
        public TruLeaderChosen() { }
        public TruLeaderChosen(string superNodeId, int sequence)
        {
            MsgType = TruSharedV2.ConsensusMsgType.LEADER_CHOSEN;
            NewSuperLeaderId = superNodeId;
        }

        public string NewSuperLeaderId { get; set; }
        public int  Sequence { get; set; }

        public override string stringifyMsg()
        {
            return MsgType + "--" + NewSuperLeaderId;
        }
    }
}
