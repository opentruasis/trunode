﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.Interfaces;

namespace TruV2Shared.TruDataUnits
{
    public class TruTxVerify : SignedMsg
    {
        public double SenderBalance { get; set; }
        public string CurrentBlockHash { get; set; } // This proves the validator is really a super node/ full node, and is synced..
        public string TxID { get; set; }
   
        public override string stringifyMsg()
        {
            return "";
        }
    }
}
