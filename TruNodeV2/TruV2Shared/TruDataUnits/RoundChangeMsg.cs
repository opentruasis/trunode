﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruSharedV2;

namespace TruV2Shared.TruDataUnits
{
    public class RoundChangeMsg : TruConsensusMsg
    {
        public RoundChangeMsg()
        {
            MsgType = ConsensusMsgType.ROUND_CHANGE;
        }

        public TruBlock Block { get; set; }

        public override string stringifyMsg()
        {
            return MsgType.ToString() + "--" + Block.stringifyMsg();
        }
    }
}
