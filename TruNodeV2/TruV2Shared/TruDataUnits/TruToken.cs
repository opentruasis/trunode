﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruToken
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string FileExt { get; set; }

        public bool ForSale { get; set; } = false;

        public double SalePrice { get; set; } = 0;

        public string CreatedBy { get; set; }

        public TruToken()
        {
            ID = Guid.NewGuid().ToString();
        }
    }
}
