﻿using System;
using System.Collections.Generic;

namespace TruV2Shared.TruDataUnits
{
    public class TruApp
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<string> ShowcaseFiles { get; set; } = new List<string>();

        public string Program { get; set; }

        public string GitRepoURL { get; set; }

        public string Hash { get; set; }

        public string CreatedBy { get; set; }


        public TruApp()
        {
            ID = Guid.NewGuid().ToString();
        }
    }
}