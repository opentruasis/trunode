﻿using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.DTOs;

namespace TruV2Shared.TruDataUnits
{
    public class SuperNodeInfo : TruDTO
    {
  
        public string ID { get; set; }

        public string PubKey { get; set; }

        public long endblockID { get; set; }

        public string currentHash { get; set; }

        public int Heartbeat { get; set; }

        public string SuperURL { get; set; }

        public int connectedFullNodeCount { get; set; } // helps determine expected tx verification msgs from clients

        public int WalletCount { get; set; }

        public int SuperUrlListCount { get; set; }
         
        public string SoccetConnectionID { get; set; }
   
        public TruMsgType Type { get => TruMsgType.SuperHeartBeat; set { } }
        public string SenderId { get; set; }
        public string ConnectionID { get; set; }

        [JsonIgnore]
        public HubConnection WebSoccket { get; set; }

    }
}
