﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TruV2Shared.TruDataUnits
{
    public class TruSuperVote : TruConsensusMsg
    {
        public TruSuperVote()
        {
            MsgType = TruSharedV2.ConsensusMsgType.LEADER_VOTE;
        }
 
        public long ElectionCycle { get; set; }

        public int Sequence { get; set; }

        public override string stringifyMsg()
        {
            return MsgType + "--" + Sequence + "-+" + ElectionCycle;
        }
    }
}
