﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.Interfaces;

namespace TruV2Shared.TruDataUnits
{
    public class BlockConsensus : SignedMsg
    {
        public TruBlock LatestBlock {get; set;}
        public string SuperURL { get; set; }

        public override string stringifyMsg()
        {
            var txStr = JsonConvert.SerializeObject(new Dictionary<string, dynamic>{
                {"LatestBlock",  JsonConvert.SerializeObject(LatestBlock) },
                {"SuperURL", SuperURL },
                {"SignerId", this.SignerId }
            });

            return txStr;
        }
    }
}
