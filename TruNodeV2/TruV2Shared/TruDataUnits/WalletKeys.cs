﻿using Newtonsoft.Json;

namespace TruV2Shared.TruDataUnits
{
    public class WalletKeys
    {
        public WalletKeys() { }
 
        public string PubKey { get; set; } = "";
 
        public string PrivKey { get; set; } = "";


        [JsonIgnore]
        public bool Private = true;

        public bool ShouldSerializePrivKey()
        {
            return !Private;
        }

    }
}
