﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public class RunningAd
    {
        public string _id;
        public int views;
        public int viewCredits;
        public List<string> updatedOn;
        public string name;
        public string fileName;
        public string link;
        public int status;
    }
}
