﻿using System;
using System.Collections.Generic;
using System.Text;
 

namespace TruShared
{
    public class JobTypeSchema
    {
        public string _id;
        public string name;
        public string description;
        public string jobCode;
        public dynamic parameters;
        public double computeCost;
    }
}

