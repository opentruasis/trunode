﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public class NeruoNetInferenceResult : JobResult
    {
        public string InferenceResultFile { get; set; }
    }
}
