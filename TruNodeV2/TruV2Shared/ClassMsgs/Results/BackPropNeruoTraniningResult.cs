﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public class BackPropNeruoTraniningResult : JobResult
    {
        public string ModelFile { get; set; }
        public double Error { get; set; }
    }
}
