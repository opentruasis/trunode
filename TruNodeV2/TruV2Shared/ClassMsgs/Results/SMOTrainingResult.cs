﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public class SMOTrainingResult : JobResult
    {
       
        public SMOTrainingResult(byte[] kernel)
        {
            this.kernel = kernel;

        }

        public byte[] kernel;

    }
}
