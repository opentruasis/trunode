﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public class JobResult : IJobResult
    {
        public double Runtime { get; set; }
        public string ResultType { get { return this.GetType().ToString(); } set { } }
    }
}
