﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public class ActiveJob
    {
        public string _id;
        public List<string> updatedOn;
        public string name;
        public string typeId;
        public List<JobParams> parameters;
        public double progress;
        public int priority;
        public int status;
        public double computeCredits;
    }
}
