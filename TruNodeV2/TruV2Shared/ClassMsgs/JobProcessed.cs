﻿using System;
using System.Collections.Generic;
using System.Text;
using TruShared;

namespace TruShared
{
    public class JobProcessed
    {
        public string _id;
        public string processedOn;
        public dynamic jobResult;
    }
}
