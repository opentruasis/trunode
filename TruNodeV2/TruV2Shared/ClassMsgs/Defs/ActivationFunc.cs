﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared.ClassMsgs.Defs
{
    public class ActivationFunc
    {
        public string ActivationType { get; set; }

        public List<double> FuncParams { get; set; }

        public int InputCount { get; set; }

        public List<int> LayerDef { get; set; }

    }
}
