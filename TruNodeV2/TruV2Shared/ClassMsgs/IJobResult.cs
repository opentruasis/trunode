﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruShared
{
    public interface IJobResult
    {
        string ResultType { get; set; }
        double Runtime { get; set; }
    }
}
