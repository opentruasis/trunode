﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TruV2Shared
{
    interface ITruCrypto
    {
        string PrivateKeyEncoded { get; } // private key

        string AddressEncoded { get;  } // public key
  
        byte[] Encrypt(byte[] msg, string InjectedPubKey);

        byte[] Decrypt(byte[] msg);
 
        string Serialize();  // to json

        bool Deserialize(string walletData);

    }
}
