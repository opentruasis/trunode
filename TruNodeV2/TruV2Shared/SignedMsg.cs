﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TruV2Shared.Interfaces;

namespace TruV2Shared
{
    public abstract class SignedMsg : ISignedMsg
    {
        public string SignerId { get; set; }
        public string Hash { get; set; }
        public string Signature { get; set; }

        public virtual bool isValidSig(string injectedPubKeyBase64Enc)
        {
            try
            {
                var msgString = this.stringifyMsg();
                return TruRSACrypto.VerifyHash(
                       msgString,
                       this.Signature,
                       injectedPubKeyBase64Enc
                       );

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public abstract string stringifyMsg();
    }
}
